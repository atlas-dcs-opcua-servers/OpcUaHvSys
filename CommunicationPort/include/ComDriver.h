#ifndef _COMDRIVER_H_
#define _COMDRIVER_H_


#include <map>
#include <string>
#include "ComMessage.h"
// #include "srvtrace.h"         *JO*    probably not needed => LogIt.h
#include "opcua_baseobjecttype.h"    // *JO*   for UaTypes
#include <LogIt.h>
#ifdef WIN32
#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <windows.h>
#else
#include <termios.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <errno.h>
#endif

using namespace std;


#define MAX_COM_READ_RETRY 3
#define MAX_COM_WRITE_RETRY 2
#define MAX_REQ_RETRY 1
//#define PURGE_COM_SLEEP_TIME 100
#define READ_COM_RETRY_SLEEP_TIME 20
#define REQ_RETRY_SLEEP_TIME 10
#define READ_RESPONSE_SLEEP_TIME 30

enum IO_DIR {
	 IO_IN    = 0,
	 IO_OUT   = 1,
	 IO_INOUT = 3};

// using namespace AddressSpace;    *JO* probably not needed

class CComDriver  
{
public:
	CComDriver();
	virtual ~CComDriver(void);
	UaStatus iniPort(string, unsigned long, unsigned char, unsigned char, unsigned char, 
			         unsigned long, unsigned long, unsigned long, unsigned long, unsigned long, int*);
	UaStatus setCOMstate(int, unsigned long, unsigned char, unsigned char, unsigned char);
	UaStatus setCOMTimeouts(int, unsigned long, unsigned long, unsigned long, unsigned long, unsigned long);
	bool isSupportedParameter(char*, int);
	int getCOMParameter(const string&, int);
	UaStatus getPortError(int);
	void sendMessage(int,ComMessage*);
	unsigned char sendRespMessage(int, ComMessage*, short);
	unsigned char getMessage(int,ComMessage*, short);
	void HvSysCommPurge(int, bool, ComMessage*);
	void flushCOMPort(int, IO_DIR);
	void addToPortsMaps(int, string);
	UaStatus terminatePort(int);
#ifndef WIN32
	speed_t getSpeed(unsigned long);
#endif

private:
	map<int, string> names;
};

#endif // _COMDRIVER_H_
