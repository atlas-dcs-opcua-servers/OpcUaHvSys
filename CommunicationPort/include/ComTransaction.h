#ifndef _COMTRANSACTION_H_
#define _COMTRANSACTION_H_

#include "HVSysDefinitions.h"
#include "ComPortObject.h"
#include "uasemaphore.h"
#include "srvtrace.h"
#include <time.h>

	class ComTransaction
	{
		UA_DISABLE_COPY(ComTransaction);
	public:
		UaStatus getHVSysErrorCode(unsigned char err);
		UaStatus getWriteWord(unsigned short *, UaVariant *);
		UaStatus processComTransaction(UaVariant*);
		void updateValues(UaNode*, ItemsDefs*, UaStatus*);
		UaVariant getValue(ItemsDefs*);
		void initComTransaction(UaVariant*, bool);
		void initMonTransaction(FUNCTION_ID);
		void initMonTransaction(unsigned char, unsigned char,FUNCTION_ID);
		void setCalib(float, float, float, float);
		bool getBoolVal(int index) { return m_bValues[index];}
		float getFloatVal(int index) { return m_fValues[index];}
		short getShortVal(void) { return m_sValue; }
		unsigned short getUShortVal(void) { return (unsigned short)m_sValue; }
		time_t getTimeStamp() { return m_timestamp; }
		ComTransaction(UaNodeId&, std::vector<HW_ITEM_ATTRIBUTES>::iterator, ComPortObject*);
		ComTransaction(const unsigned char *cb, const unsigned char *cc, std::vector<HW_ITEM_ATTRIBUTES>::iterator, ComPortObject*);
		ComTransaction(ComPortObject*);
		virtual ~ComTransaction();
		UaStatus m_initResult;
		void setResponseID();

	private:
		UaSemaphore			m_semaphore;
		ComPortObject*		m_pcp;
	protected:
		bool				m_bRead;
		bool				m_simple;
		unsigned char		m_RCommand; //m_Command
		unsigned char		m_WCommand; //m_Command
		FUNCTION_ID			m_RfunctionID; //m_functionID;
		FUNCTION_ID			m_WfunctionID; //m_functionID;
		RESPONSE_ID			m_responseID;
		CALIB_TYPE			m_CalibType; // setlinear, monlinear, NoCalib
		float				m_Slope;
		float				m_Offset; 
		UaString			m_PortID;
		OpcUa_BuiltInType	m_itemType;
		unsigned char		m_BranchID;
		unsigned char		m_CellID;
		unsigned char		m_CellSubAddr;
		float				m_fValues[8];
		short				m_sValue;
		bool				m_bValues[16];
		OpcUa_UInt32		m_uint32;
		OpcUa_Boolean       m_ubool;
		UaString            m_ustring;
		UaDataValue			m_lastValue;		
		time_t              m_timestamp;
		int					m_dataIndex;
	};
#endif //_COMTRANSACTION_H_
