#ifndef _COMMESSAGE_H_
#define _COMMESSAGE_H_


//  #include "HVSysDefinitions.h"   *JO*  should be resolved by nodes
//  using namespace AddressSpace;   *JO*  probably not necessary
#include "HVSysDefinitions.h"

class ComMessage 
{
	UA_DISABLE_COPY(ComMessage);
public:
	ComMessage(){
		TxBytes = 0;
		RxBytes = 0;
		int i;
		for( i=0; i<10; i++) m_TxData[i] = '\0';
		for( i=0; i<30; i++) m_RxData[i] = '\0';
		TxResult = OpcUa_BadWaitingForInitialData;
		RxResult = OpcUa_BadWaitingForInitialData;
		m_Error = ' ';
	};
	virtual ~ComMessage(void){};

	unsigned char m_TxData[10];
	unsigned char m_RxData[30];
	void setTxDataByte(unsigned char data, int i) { m_TxData[i] = data; }
	void setTxBytes(unsigned long n) {TxBytes = n; }
	void setTxResult(UaStatus result) { TxResult = result; }
	void setError(unsigned char error) { m_Error = error; }
	void setRxDataByte(unsigned char data, int i) { m_RxData[i] = data; }
	void setRxBytes(unsigned long n) { RxBytes = n; }
	void setRxResult(UaStatus result) { RxResult = result; }
	unsigned char getTxDataByte(int i) { return m_TxData[i]; }
	unsigned long getTxBytes() { return TxBytes; }
	UaStatus getTxResult() { return TxResult; }
	unsigned char getError() { return m_Error; }
	unsigned char getRxDataByte(int i) { return m_RxData[i]; }
	unsigned long getRxBytes() { return RxBytes; }
	UaStatus getRxResult() { return RxResult; }

private:
	unsigned long 	TxBytes;
	unsigned long 	RxBytes;
	UaStatus 		TxResult;
	UaStatus 		RxResult;
	unsigned char	m_Error;
};

#endif

