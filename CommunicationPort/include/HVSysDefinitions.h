#ifndef __HVSYSDEFINITIONS_H__
#define __HVSYSDEFINITIONS_H__
#include "uaobjecttypes.h"
#include "TypeIds.h"
#include <string>
#include <vector>

#define Rx_QUEUE_LENGHT 		2100
#define Tx_QUEUE_LENGHT   		20
#define MON_EMPTY_CYCLES_CONST 	42
#define MON_EMPTY_SLEEP_TIME 	200000    //in microseconds
#ifndef WIN32
#define NOPARITY 	0x00
#define ODDPARITY 	0x01
#define EVENPARITY 	0x02
#define ONESTOPBIT 	0x00
#define TWOSTOPBITS 0x02 
#endif


using namespace std;
//namespace Device   //*JO*
//{  // *JO*

enum FUNCTION_ID 
	{fNo		=	0, 
	 fP			=	1,
	 f3			=	2,
	 fM			=	3,
	 fH			=	4,
	 fQ			=	5,
	 fBVswitch	=	6,
	 fLVswitch	=   7,
	 fZ			=   8,
	 fR			=   9,
	 fB			=  10,
	 fCellMon	=  11,
	 fCellSet	=  12,
	 fCellAll	=  13,
	 fLastValue	=  14,
	 fPortAttr	=  64,
	 fPortAction=  65,
	 fBusAttr	=  66,
	 fTraceAttr	=  67};

enum RESPONSE_ID 
	{rNo		=	0,
	 rP			=	1,
	 r3			=	2,
	 rM			=	3,
	 rH07		=	4,
	 rH10		=	5,
	 rH11		=	6,
	 rH14		=   7,
	 rQ01		=   8,
	 rQ03		=   9,
	 rQ05		=  10,
	 rQ08		=  11,
	 rQ12		=  12,
	 rCellMon	=  13,
	 rCellSet	=  14,
	 rCellAll	=  15,
	 rPortAttr	=  64,
	 rPortAction=  65,
	 rBusAttr	=  66,
	 rTraceAttr	=  67};

 enum CALIB_TYPE {
	 noCalib	=	0,
	 setLinear	=	1,
	 monLinear	=	2};
		 
 struct COM_TRANSACTION
 {
	 FUNCTION_ID fID;            // type of HVSys message
	 OpcUa_UInt32	TxBytes;     // number of Tx message Bytes
	 OpcUa_UInt32	RxBytes;     // number of Rx message Bytes (if error code == Ok)
	 short			ErrCodeType; // error coding type: 
							     // -1 means transaction returns no error code
							     //  0 means transaction returns decimal error code - Byte0
							     //  1 means transaction returns ASCII error code - 6 Bytes(0-5)
 };
 
 const COM_TRANSACTION mess_lenghts[] = {
	 {fNo,	0,	0, -1},
	 {fP,	1,	8, -1},
	 {f3,	1,	8, -1},
	 {fM,	1,	2, -1},
	 {fH,	4,	2,	0},
	 {fQ,	4,	3,	0},
	 {fBVswitch,2,1,0},
	 {fLVswitch,2,1,0},
	 {fZ,	5,	1,	0},
	 {fR,	1,	1, -1},
	 {fB,	2,	0, -1},
	 {fCellMon,5,10,1},
	 {fCellSet,4,17,1},
	 {fCellAll,4,25,1},
	 {fLastValue,0,	0, -1},
 };

 enum OFFSET_TYPE
 {
		BAUD_RATE			= 100,
		DATA_BITS			= 101,
		STOP_BITS			= 102,
		PARITY				= 103,
		CONTROLLER_TYPE		= 104,
		COMM_BUS			= 105,
		CELL_LIST			= 106,
		STACK_TRACE_ENABLE	= 200,
		STACK_TRACE_LEVEL	= 201,
		APP_TRACE_ENABLE	= 202,
		APP_TRACE_LEVEL		= 203,
		MAX_TRACE_ENTRIES	= 204,
		MAX_BACKUP_FILES	= 205,
		APP_TRACE_FILE		= 206

 };

 enum COM_DCB
 {
		READINTERVALTIMEOUT			= 201,
		READTOTALTIMEOUTCONSTANT	= 202,
		READTOTALTIMEOUTMULTIPLIER	= 203,
		WRITETOTALTIMEOUTCONSTANT	= 204,
		WRITETOTALTIMEOUTMULTIPLIER	= 205
		};
 enum HV_CONTROLLER
 {
		TYPE_SC508		= 50
		};


enum BAUD_RATE_VALUES
	{Baud_9600		=	9,
	 Baud_19200		=	2,
	 Baud_38400		=	3,
	 Baud_57600		=	5,
	 Baud_115200	=	1
	};

enum COMM_BUS_TYPES
	{RS232	=	1,
	 USB	=	2
	}; 

enum EU_TYPEtag 
	{VOLTAGE		= 0x00010001,
	CURRENT			= 0x00020001,
	TEMPERATURE		= 0x00040001,
	STATUS_BIT		= 0x00010000,
	COMMAND			= 0x00020000,
	TIME			= 0x00030001,
	STRING			= 0x00040000,
	NOENUM			= 0x00000000
	};

class RESPONSE_ITEM_ATTRIBUTES
{
public:
	RESPONSE_ITEM_ATTRIBUTES(const string itemID,
		                     const string nodeID,
		                     const _OpcUa_BuiltInType varType,
		                     const int dataIndex): itemID_(itemID), nodeID_(nodeID), varType_(varType),dataIndex_(dataIndex) {}

	virtual ~RESPONSE_ITEM_ATTRIBUTES(void) {};
	string &itemID()				{ return itemID_; }
	string &nodeID()                { return nodeID_; }
	_OpcUa_BuiltInType &varType()	{ return varType_; }
	int &dataIndex()				{ return dataIndex_; }

private:
	string itemID_;
	string nodeID_;
	_OpcUa_BuiltInType varType_;
	int dataIndex_;
};

class HW_ITEM_ATTRIBUTES
{


public:
	virtual ~HW_ITEM_ATTRIBUTES(void) {};
	string &itemID() { return itemID_; }
	string &accessLevel() { return accessLevel_; }
	OpcUa_BuiltInType &varType() { return varType_; }
	string &valueHandling() { return valueHandling_; }
	EU_TYPEtag &CustomType() { return CustomType_; }
	string &writeCommand() { return writeCommand_; }
	string &readCommand() { return readCommand_; }
	int &dataIndex() { return dataIndex_; }
	double &LoLimit() { return LoLimit_; }
	double &HiLimit() { return HiLimit_; }
	string &descr() { return descr_; }
	string &units() { return units_; }
	CALIB_TYPE &calibrationType() { return calibrationType_; }
	HW_ITEM_ATTRIBUTES(const string itemID, 
		const string accessLevel, 
		const _OpcUa_BuiltInType varType, 
		const string valueHandling,
		const EU_TYPEtag CustomType, 
		const string writeCommand, 
		const string readCommand, 
		const int dataIndex,
		const double LoLimit, 
		const double HiLimit, 
		const string descr, 
		const string units, 
		const CALIB_TYPE calibrationType):
			itemID_(itemID),
			accessLevel_(accessLevel),
			varType_(varType),
			valueHandling_(valueHandling),
			CustomType_(CustomType),
			writeCommand_(writeCommand),
			readCommand_(readCommand),
			dataIndex_(dataIndex),
			LoLimit_(LoLimit),
			HiLimit_(HiLimit),
			descr_(descr),
			units_(units),
			calibrationType_(calibrationType),
			baTypeId_(0) {}
	bool ini_defValue(UaVariant *value, _OpcUa_BuiltInType type) 
	{
		bool ret = false;
		switch(type)
		{
		case OpcUaType_Boolean:
			value->setBool(false);
			ret = true;
			break;
		case OpcUaType_Byte:
			value->setByte(0);
			ret = true;
			break;
		case OpcUaType_Float:
			value->setFloat(0);
			break;
		case OpcUaType_Double:
			value->setDouble(0);
			ret = true;
			break;
		case OpcUaType_String:
			value->setString(UaString(""));
			ret = true;
			break;
		case OpcUaType_Int16:
			value->setInt16(0);
			break;
		case OpcUaType_UInt16:
			value->setUInt16(0);
			ret = true;
			break;
		case OpcUaType_Int32:
			value->setInt32(0);
			ret = true;
			break;
		case OpcUaType_UInt32:
			value->setUInt32(0);
			ret = true;
			break;
		case OpcUaType_Int64:
			value->setInt64(0);
			break;
		case OpcUaType_UInt64:
			value->setUInt64(0);
			ret = true;
			break;
		default:
			ret = false;
		}
		return ret;
	}
	OpcUa_Byte getAccessLevel()
	{
		OpcUa_Byte ret;
		if(!strcmp(accessLevel().c_str(), "R")) ret = Ua_AccessLevel_CurrentRead;
		else if(!strcmp(accessLevel().c_str(), "W")) ret = Ua_AccessLevel_CurrentWrite;
		else if(!strcmp(accessLevel().c_str(), "RW")) ret = Ua_AccessLevel_CurrentRead | Ua_AccessLevel_CurrentWrite;
		else ret = Ua_AccessLevel_CurrentRead;
		return ret;
	}
	OpcUa_UInt32 getValueHandling()
	{
		OpcUa_UInt32 ret;
		if(!strcmp(valueHandling().c_str(), "IO_NONE")) ret = UaVariable_Value_None;
		else if(!strcmp(valueHandling().c_str(), "IO_CACHE")) ret = UaVariable_Value_Cache;
		else if(!strcmp(valueHandling().c_str(), "IO_CACHESOURCE")) ret = UaVariable_Value_CacheIsSource;
		else if(!strcmp(valueHandling().c_str(), "IO_CACHEONREQUEST")) ret = UaVariable_Value_CacheIsUpdatedOnRequest;
		return ret;
	}
private:
	string itemID_;
	string accessLevel_;
	_OpcUa_BuiltInType varType_;
	string valueHandling_;
	EU_TYPEtag CustomType_;
	string writeCommand_;
	string readCommand_;
	int	   dataIndex_;
	double LoLimit_;
	double HiLimit_;
	string descr_;
	string units_;
	CALIB_TYPE calibrationType_;
	int baTypeId_;
};
class ItemsDefs
{
public:
	ItemsDefs(unsigned int type) {
		iniDefs(type);
	};
	virtual ~ItemsDefs(void) {};
	std::vector<HW_ITEM_ATTRIBUTES> iaDefs;
	std::multimap<RESPONSE_ID,RESPONSE_ITEM_ATTRIBUTES>respItems;
	
	void iniDefs(unsigned int type)
	{
		switch(type) {
		case Ba_RootType:
		//void UaRootObject::ini_HVSysItemDefs() {}
			//                                   ItemName			Access	ItemType			Handling		EU_Type	IO_W			IO_R		dataIndex				min		max		Description	Unit	calibration
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("StackTraceEnable",	"RW",	OpcUaType_Boolean,	"IO_CACHE",	NOENUM,	"TraceAttr",	"TraceAttr",	STACK_TRACE_ENABLE,	0.0,	0.0,	"",			"",		noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("StackTraceLevel",	"RW",	OpcUaType_String,	"IO_CACHE",	NOENUM,	"TraceAttr",	"TraceAttr",	STACK_TRACE_LEVEL,	0.0,	0.0,	"",			"",		noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("AppTraceEnable",	"RW",	OpcUaType_Boolean,	"IO_CACHE",	NOENUM,	"TraceAttr",	"TraceAttr",	APP_TRACE_ENABLE,	0.0,	0.0,	"",			"",		noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("AppTraceLevel",	"RW",	OpcUaType_String,	"IO_CACHE",	NOENUM,	"TraceAttr",	"TraceAttr",	APP_TRACE_LEVEL,	0.0,	0.0,	"",			"",		noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("MaxTraceEntries",	"RW",	OpcUaType_UInt32,	"IO_CACHE",	NOENUM,	"TraceAttr",	"TraceAttr",	MAX_TRACE_ENTRIES,	0.0,	0.0,	"",			"",		noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("MaxBackupFiles",	"RW",	OpcUaType_UInt32,	"IO_CACHE",	NOENUM,	"TraceAttr",	"TraceAttr",	MAX_BACKUP_FILES,	0.0,	0.0,	"",			"",		noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("AppTraceFile",		"RW",	OpcUaType_String,	"IO_CACHE",	STRING,	"TraceAttr",	"TraceAttr",	APP_TRACE_FILE,		0.0,	0.0,	"",			"",		noCalib));

		//void UaRootObject::ini_HVSysResponseDefs() {}
		
		//																						itemID,	nodeID,	varType,			dataIndex
		//respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rNo,RESPONSE_ITEM_ATTRIBUTES("",	"",	    OpcUaType_Float,	0)));
		break;

		case Ba_PortType:
		// void UaPortObject::ini_HVSysItemDefs(void) {}
		//                                   ItemName			Access	ItemType			Handling			EU_Type	IO_W		IO_R		dataIndex	min		max		Description	Unit	calibration
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("BaudRate",			"RW",	OpcUaType_Int16,	"IO_CACHESOURCE",	NOENUM,	"PortAttr",	"PortAttr",	BAUD_RATE,	0.0,	0.0,	"",			"",		noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("DataBits",			"R",	OpcUaType_Int16,	"IO_CACHESOURCE",	NOENUM,	"PortAttr",	"PortAttr",	DATA_BITS,	0.0,	0.0,	"",			"",		noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("StopBits",			"R",	OpcUaType_String,	"IO_CACHESOURCE",	NOENUM,	"PortAttr",	"PortAttr",	STOP_BITS,	0.0,	0.0,	"",			"",		noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("Parity",			"R",	OpcUaType_String,	"IO_CACHESOURCE",	NOENUM,	"PortAttr",	"PortAttr",	PARITY,		0.0,	0.0,	"",			"",		noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("CommunicationBus",	"RW",	OpcUaType_String,	"IO_CACHE",			STRING,	"B",		"BusAttr",	COMM_BUS,	0.0,	0.0,	"",			"",		noCalib));

		//void UaPortObject::ini_HVSysResponseDefs(void) {}
		//																						  itemID,	      nodeID,		varType,			dataIndex
		/*P*/	// Monitoring Thread
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rP,RESPONSE_ITEM_ATTRIBUTES("BaseVoltValue", "Branch0",	OpcUaType_Float,	0 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rP,RESPONSE_ITEM_ATTRIBUTES("BaseVoltValue", "Branch1",	OpcUaType_Float,	1 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rP,RESPONSE_ITEM_ATTRIBUTES("BaseVoltValue", "Branch2",	OpcUaType_Float,	2 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rP,RESPONSE_ITEM_ATTRIBUTES("BaseVoltValue", "Branch3",	OpcUaType_Float,	3 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rP,RESPONSE_ITEM_ATTRIBUTES("LowVoltValue" , "Branch0",	OpcUaType_Float,	4 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rP,RESPONSE_ITEM_ATTRIBUTES("LowVoltValue" , "Branch1",	OpcUaType_Float,	5 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rP,RESPONSE_ITEM_ATTRIBUTES("LowVoltValue" , "Branch2",	OpcUaType_Float,	6 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rP,RESPONSE_ITEM_ATTRIBUTES("LowVoltValue" , "Branch3",	OpcUaType_Float,	7 )));
		/*3*/	// Monitoring Thread
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(r3,RESPONSE_ITEM_ATTRIBUTES("ExtVoltValue",    "Crate",	OpcUaType_Float,	0 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(r3,RESPONSE_ITEM_ATTRIBUTES("ExtVoltRef",      "Crate",	OpcUaType_Float,	1 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(r3,RESPONSE_ITEM_ATTRIBUTES("IntTemp",	        "Crate",	OpcUaType_Float,	2 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(r3,RESPONSE_ITEM_ATTRIBUTES("ExtTempA",	    "Crate",	OpcUaType_Float,	3 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(r3,RESPONSE_ITEM_ATTRIBUTES("ExtTempB",	    "Crate",	OpcUaType_Float,	4 )));
		/*M*/	// Monitoring Thread
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("BaseVoltStatus",  "Branch0",  OpcUaType_Boolean,	0 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("BaseVoltStatus",  "Branch1",	OpcUaType_Boolean,	1 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("BaseVoltStatus",  "Branch2",	OpcUaType_Boolean,	2 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("BaseVoltStatus",  "Branch3",	OpcUaType_Boolean,	3 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("LowVoltStatus",   "Branch0",	OpcUaType_Boolean,	4 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("LowVoltStatus",   "Branch1",	OpcUaType_Boolean,	5 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("LowVoltStatus",   "Branch2",	OpcUaType_Boolean,	6 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("LowVoltStatus",   "Branch3",	OpcUaType_Boolean,	7 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("ExtHVEnableStatus", "Crate",	OpcUaType_Boolean,	8 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("OverheatingStatus",  "Crate",	OpcUaType_Boolean,	9 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("BVOverheatTrip",    "Crate",	OpcUaType_Boolean,	10 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("ExtRefStatus",      "Crate",	OpcUaType_Boolean,	11 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("BVINLLTrip",	      "Crate",	OpcUaType_Boolean,	12 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("BVINULTrip",        "Crate",	OpcUaType_Boolean,	13 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("BREFLLTrip",        "Crate",	OpcUaType_Boolean,	14 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("BREFULTrip",        "Crate",	OpcUaType_Boolean,	15 )));
		break;
		case Ba_CrateType:
		//void UaCrateObject::ini_HVSysItemDefs(void){}
		//                                   ItemName			Access	ItemType			Handling			EU_Type		IO_W	IO_R dataIndex	min		max		Description	Unit		calibration
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("ExtVoltValue",		"R",	OpcUaType_Float,	"IO_CACHESOURCE",	VOLTAGE,	"",		"3",	0,		0.0,	0.0,	"",			"Volt",		noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("ExtVoltRef",		"R",	OpcUaType_Float,	"IO_CACHESOURCE",	VOLTAGE,	"",		"3",	1,		0.0,	0.0,	"",			"Volt",		noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("IntTemp",			"R",	OpcUaType_Float,	"IO_CACHESOURCE",	TEMPERATURE,"",		"3",	2,		0.0,	0.0,	"",			"C deg.",	noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("ExtTempA",			"R",	OpcUaType_Float,	"IO_CACHESOURCE",	TEMPERATURE,"",		"3",	3,		0.0,	0.0,	"",			"C deg.",	noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("ExtTempB",			"R",	OpcUaType_Float,	"IO_CACHESOURCE",	TEMPERATURE,"",		"3",	4,		0.0,	0.0,	"",			"C deg.",	noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("BREFSwitch",		"RW",	OpcUaType_Boolean,	"IO_CACHE",			COMMAND,	"EO",	"LastValue",0,		0.0,	0.0,	"",			"",			noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("ExtHVEnableStatus","R",	OpcUaType_Boolean,	"IO_CACHESOURCE",	STATUS_BIT,	"",		"M",	8,		0.0,	0.0,	"",			"",			noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("OverheatingStatus",	"R",	OpcUaType_Boolean,	"IO_CACHESOURCE",	STATUS_BIT,	"",		"M",	9,		0.0,	0.0,	"",			"",			noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("BVOverheatTrip",	"R",	OpcUaType_Boolean,	"IO_CACHESOURCE",	STATUS_BIT,	"",		"M",	10,		0.0,	0.0,	"",			"",			noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("ExtRefStatus",		"R",	OpcUaType_Boolean,	"IO_CACHESOURCE",	STATUS_BIT,	"",		"M",	11,		0.0,	0.0,	"",			"",			noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("BVINLLTrip",		"R",	OpcUaType_Boolean,	"IO_CACHESOURCE",	STATUS_BIT,	"",		"M",	12,		0.0,	0.0,	"",			"",			noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("BVINULTrip",		"R",	OpcUaType_Boolean,	"IO_CACHESOURCE",	STATUS_BIT,	"",		"M",	13,		0.0,	0.0,	"",			"",			noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("BREFLLTrip",		"R",	OpcUaType_Boolean,	"IO_CACHESOURCE",	STATUS_BIT,	"",		"M",	14,		0.0,	0.0,	"",			"",			noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("BREFULTrip",		"R",	OpcUaType_Boolean,	"IO_CACHESOURCE",	STATUS_BIT,	"",		"M",	15,		0.0,	0.0,	"",			"",			noCalib));

		//void UaCrateObject::ini_HVSysResponseDefs(void) {}
		//																						  itemID,			nodeID, 	 varType,			dataIndex
		/*P*/	// Monitoring Thread
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rP,RESPONSE_ITEM_ATTRIBUTES("BaseVoltValue", "../Branch0",	 OpcUaType_Float,	0 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rP,RESPONSE_ITEM_ATTRIBUTES("BaseVoltValue", "../Branch1",	 OpcUaType_Float,	1 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rP,RESPONSE_ITEM_ATTRIBUTES("BaseVoltValue", "../Branch2",	 OpcUaType_Float,	2 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rP,RESPONSE_ITEM_ATTRIBUTES("BaseVoltValue", "../Branch3",	 OpcUaType_Float,	3 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rP,RESPONSE_ITEM_ATTRIBUTES("LowVoltValue",  "../Branch0",	 OpcUaType_Float,	4 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rP,RESPONSE_ITEM_ATTRIBUTES("LowVoltValue",  "../Branch1",	 OpcUaType_Float,	5 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rP,RESPONSE_ITEM_ATTRIBUTES("LowVoltValue",  "../Branch2",	 OpcUaType_Float,	6 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rP,RESPONSE_ITEM_ATTRIBUTES("LowVoltValue",  "../Branch3",	 OpcUaType_Float,	7 )));
		/*3*/	// Monitoring Thread
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(r3,RESPONSE_ITEM_ATTRIBUTES("ExtVoltValue",	"", 		 OpcUaType_Float,	0 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(r3,RESPONSE_ITEM_ATTRIBUTES("ExtVoltRef",		"", 		 OpcUaType_Float,	1 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(r3,RESPONSE_ITEM_ATTRIBUTES("IntTemp",			"", 		 OpcUaType_Float,	2 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(r3,RESPONSE_ITEM_ATTRIBUTES("ExtTempA",		"", 		 OpcUaType_Float,	3 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(r3,RESPONSE_ITEM_ATTRIBUTES("ExtTempB",		"", 		 OpcUaType_Float,	4 )));
		/*M*/	// Monitoring Thread
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("BaseVoltStatus", "../Branch0", OpcUaType_Boolean,	0 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("BaseVoltStatus", "../Branch1", OpcUaType_Boolean,	1 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("BaseVoltStatus", "../Branch2", OpcUaType_Boolean,	2 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("BaseVoltStatus", "../Branch3", OpcUaType_Boolean,	3 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("LowVoltStatus",  "../Branch0", OpcUaType_Boolean,	4 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("LowVoltStatus",  "../Branch1", OpcUaType_Boolean,	5 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("LowVoltStatus", "../Branch2", OpcUaType_Boolean,	6 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("LowVoltStatus", "../Branch3", OpcUaType_Boolean,	7 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("ExtHVEnableStatus", "",		 OpcUaType_Boolean,	8 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("OverheatingStatus",  "",		 OpcUaType_Boolean,	9 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("BVOverheatTrip",	  "",		 OpcUaType_Boolean,	10 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("ExtRefStatus",	  "",		 OpcUaType_Boolean,	11 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("BVINLLTrip",		  "",		 OpcUaType_Boolean,	12 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("BVINULTrip",		  "",		 OpcUaType_Boolean,	13 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("BREFLLTrip",		  "",		 OpcUaType_Boolean,	14 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rM,RESPONSE_ITEM_ATTRIBUTES("BREFULTrip",		  "",		 OpcUaType_Boolean,	15 )));
		break;
		case Ba_BranchType:
		// void UaBranchObject::ini_HVSysItemDefs(void){}
		//                                   ItemName			Access	ItemType			Handling			EU_Type		IO_W	IO_R dataIndex min		max			Description	Unit	calibration
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("BaseVoltValue",	"R",	OpcUaType_Float,	"IO_CACHESOURCE",	VOLTAGE,	"",		"P",	0,		0.0,	0.0,		"",			"Volt",	noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("BowVoltValue",		"R",	OpcUaType_Float,	"IO_CACHESOURCE",	VOLTAGE,	"",		"P",	4,		0.0,	0.0,		"",			"Volt",	noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("BaseVoltSwitch",	"RW",	OpcUaType_Boolean,	"IO_CACHE",			COMMAND,	"EO",	"LastValue",0,		0.0,	0.0,		"",			"",		noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("LowVoltSwitch",	"RW",	OpcUaType_Boolean,	"IO_CACHE",			COMMAND,	"_#",	"LastValue",		0,		0.0,	0.0,		"",			"",		noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("BaseVoltStatus",	"R",	OpcUaType_Boolean,	"IO_CACHESOURCE",	STATUS_BIT,	"",		"M",	0,		0.0,	0.0,		"",			"",		noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("LowVoltStatus",	"R",	OpcUaType_Boolean,	"IO_CACHESOURCE",	STATUS_BIT,	"",		"M",	4,		0.0,	0.0,		"",			"",		noCalib));
		//void UaBranchObject::ini_HVSysResponseDefs(void) {}
		//																						  itemID, nodeID,	varType,			dataIndex
		//respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rNo,RESPONSE_ITEM_ATTRIBUTES("",	"",	OpcUaType_Float,	0 )));
		break;
		case Ba_BoardType:
		// void UaBoardObject::ini_HVSysItemDefs(void) {}
		//                                   ItemName	Access	ItemType			Handling			EU_Type	IO_W	IO_R	dataIndex		min		max		Description	Unit	calibration
		//iaDefs.push_back(HW_ITEM_ATTRIBUTES("CalibFile","RW",	OpcUaType_String,	"IO_CACHESOURCE",	NOENUM,	"",		"",		CONTROLLER_TYPE,0.0,	0.0,	"",			"",		noCalib ));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("CalibrationLabel","RW",	OpcUaType_String,	"IO_CACHESOURCE",	NOENUM,	"",		"",		CONTROLLER_TYPE,0.0,	0.0,	"",			"",		noCalib ));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("CalibrationDate","RW",	OpcUaType_String,	"IO_CACHESOURCE",	NOENUM,	"",		"",		CONTROLLER_TYPE,0.0,	0.0,	"",			"",		noCalib ));

		//void UaBoardObject::ini_HVSysResponseDefs(void) {}
		//																						        itemID,	          nodeID, varType,			dataIndex
		/*c*/	// Monitoring Thread
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellMon,RESPONSE_ITEM_ATTRIBUTES("Error",	          "", OpcUaType_Boolean,	0 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellMon,RESPONSE_ITEM_ATTRIBUTES("GeneratorStatus", "", OpcUaType_Boolean,	1 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellMon,RESPONSE_ITEM_ATTRIBUTES("AccumError",	  "", OpcUaType_Boolean,	2 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellMon,RESPONSE_ITEM_ATTRIBUTES("CurrentOverload", "", OpcUaType_Boolean,	3 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellMon,RESPONSE_ITEM_ATTRIBUTES("StandbyStatus",	  "", OpcUaType_Boolean,	4 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellMon,RESPONSE_ITEM_ATTRIBUTES("RampDownStatus",  "", OpcUaType_Boolean,	6 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellMon,RESPONSE_ITEM_ATTRIBUTES("RampUpStatus",	  "", OpcUaType_Boolean,	7 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellMon,RESPONSE_ITEM_ATTRIBUTES("VoltageValue",	  "", OpcUaType_Float,	0 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellMon,RESPONSE_ITEM_ATTRIBUTES("CurrentValue",	  "", OpcUaType_Float,	1 )));

		/*=*/	// Monitoring Thread
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellSet,RESPONSE_ITEM_ATTRIBUTES("HVSetpoint",	      "", OpcUaType_Float,	0 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellSet,RESPONSE_ITEM_ATTRIBUTES("ThresholdSetpoint",   "", OpcUaType_Float,	1 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellSet,RESPONSE_ITEM_ATTRIBUTES("RampUpSetpoint",      "", OpcUaType_Float,	2 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellSet,RESPONSE_ITEM_ATTRIBUTES("RampDownSetpoint",    "", OpcUaType_Float,	3 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellSet,RESPONSE_ITEM_ATTRIBUTES("StandbyVoltSetpoint", "", OpcUaType_Float,	4 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellSet,RESPONSE_ITEM_ATTRIBUTES("ProtectDelaySetpoint","", OpcUaType_Float,	6 )));

		/***/	// Monitoring Thread
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("HVSetpoint",	      "", OpcUaType_Float,	0 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("ThresholdSetpoint",   "", OpcUaType_Float,	1 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("RampUpSetpoint",      "", OpcUaType_Float,	2 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("RampDownSetpoint",    "", OpcUaType_Float,	3 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("StandbyVoltSetpoint", "", OpcUaType_Float,	4 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("ProtectDelaySetpoint","", OpcUaType_Float,	6 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("Error",				  "", OpcUaType_Boolean, 0 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("GeneratorStatus",	  "", OpcUaType_Boolean, 1 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("AccumError",		  "", OpcUaType_Boolean, 2 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("CurrentOverload",	  "", OpcUaType_Boolean, 3 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("StandbyStatus",		  "", OpcUaType_Boolean, 4 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("RampDownStatus",	  "", OpcUaType_Boolean, 6 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("RampUpStatus",		  "", OpcUaType_Boolean, 7 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("VoltageValue",		  "", OpcUaType_Float,	7 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("CurrentValue",		  "", OpcUaType_Float,	8 )));
		break;
		case Ba_CellType:
				
		//void UaCellObject::ini_HVSysItemDefs(void) {}
		//                                   ItemName				Access	ItemType			Handling			EU_Type		IO_W	IO_R dataIndex	min		max			Description								Unit			calibration
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("HVSetpoint",			"RW",	OpcUaType_Float,	"IO_CACHE",			VOLTAGE,	"Z.01", "Q.01",	0,		500.0,	1998.695,	"Output Voltage Setpoint",				"Volt",			setLinear));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("ThresholdSetpoint",	"RW",	OpcUaType_Float,	"IO_CACHE",			CURRENT,	"Z.03", "Q.03",	0,		0.0,	3000.0498,	"Current protection threshold",			"microAmper",	noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("VoltageValue",			"R",	OpcUaType_Float,	"IO_CACHESOURCE",	VOLTAGE,	"",		"Q.05",	0,		0.0,	0.0,		"Outupt Voltage Value",					"Volt",			monLinear));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("CurrentValue",			"R",	OpcUaType_Float,	"IO_CACHESOURCE",	CURRENT,	"",		"Q.08",	0,		0.0,	0.0,		"Output Current Value",					"microAmper",	noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("RampUpSetpoint",		"RW",	OpcUaType_Float,	"IO_CACHE",			VOLTAGE,	"Z.10",	"H.10",	0,		-1.0,	58.6,		"Switches off cell ramping up if -1",	"Volt/second",	noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("RampDownSetpoint",		"RW",	OpcUaType_Float,	"IO_CACHE",			VOLTAGE,	"Z.11",	"H.11",	0,		-1.0,	58.6,		"Switches off cell ramping down if -1",	"Volt/second",	noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("StandbyVoltSetpoint",	"RW",	OpcUaType_Float,	"IO_CACHE",			VOLTAGE,	"Z.12",	"Q.12",	0,		500.0,	1998.695,	"Switches off HV cell generator if -1",	"Volt",			noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("ProtectDelaySetpoint",	"RW",	OpcUaType_Int16,	"IO_CACHE",			TIME,		"Z.14",	"H.14",	0,		-1.0,	12700.0,	"No delay if -1",						"milisecond",	noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("CellSwitch",			"RW",	OpcUaType_Boolean,	"IO_CACHE",			COMMAND,	"Z.00",	"LastValue",0,		0.0,	0.0,		"Switch on/off cell",					"",				noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("Error",				"R",	OpcUaType_Boolean,	"IO_CACHESOURCE",	STATUS_BIT,	"",		"H.07",	0,		0.0,	0.0,		"",										"",				noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("GeneratorStatus",		"R",	OpcUaType_Boolean,	"IO_CACHESOURCE",	STATUS_BIT,	"",		"H.07",	1,		0.0,	0.0,		"",										"",				noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("AccumError",			"R",	OpcUaType_Boolean,	"IO_CACHESOURCE",	STATUS_BIT,	"",		"H.07",	2,		0.0,	0.0,		"",										"",				noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("CurrentOverload",		"R",	OpcUaType_Boolean,	"IO_CACHESOURCE",	STATUS_BIT,	"",		"H.07",	3,		0.0,	0.0,		"",										"",				noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("StandbyStatus",		"R",	OpcUaType_Boolean,	"IO_CACHESOURCE",	STATUS_BIT,	"",		"H.07",	4,		0.0,	0.0,		"",										"",				noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("RampDownStatus",		"R",	OpcUaType_Boolean,	"IO_CACHESOURCE",	STATUS_BIT,	"",		"H.07",	6,		0.0,	0.0,		"",										"",				noCalib));
		iaDefs.push_back(HW_ITEM_ATTRIBUTES("RampUpStatus",			"R",	OpcUaType_Boolean,	"IO_CACHESOURCE",	STATUS_BIT,	"",		"H.07",	7,		0.0,	0.0,		"",										"",				noCalib));

		//void UaCellObject::ini_HVSysResponseDefs(void) {}
		//																								itemID,	nodeID,	  varType,			   dataIndex

		/*c*/	// Monitoring Thread
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellMon,RESPONSE_ITEM_ATTRIBUTES("Error",			 	"", OpcUaType_Boolean,	0 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellMon,RESPONSE_ITEM_ATTRIBUTES("GeneratorStatus",	"", OpcUaType_Boolean,	1 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellMon,RESPONSE_ITEM_ATTRIBUTES("AccumError",	 	"", OpcUaType_Boolean,	2 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellMon,RESPONSE_ITEM_ATTRIBUTES("CurrentOverload",	"", OpcUaType_Boolean,	3 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellMon,RESPONSE_ITEM_ATTRIBUTES("StandbyStatus",	 	"", OpcUaType_Boolean,	4 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellMon,RESPONSE_ITEM_ATTRIBUTES("RampDownStatus", 	"", OpcUaType_Boolean,	6 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellMon,RESPONSE_ITEM_ATTRIBUTES("RampUpStatus",	 	"", OpcUaType_Boolean,	7 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellMon,RESPONSE_ITEM_ATTRIBUTES("VoltageValue",	 	"", OpcUaType_Float,	0 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellMon,RESPONSE_ITEM_ATTRIBUTES("CurrentValue",	 	"", OpcUaType_Float,	1 )));

		/*=*/	// Monitoring Thread
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellSet,RESPONSE_ITEM_ATTRIBUTES("HVSetpoint",		"",	OpcUaType_Float,	0 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellSet,RESPONSE_ITEM_ATTRIBUTES("ThresholdSetpoint",	"",	OpcUaType_Float,	1 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellSet,RESPONSE_ITEM_ATTRIBUTES("RampUpSetpoint",	"",	OpcUaType_Float,	2 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellSet,RESPONSE_ITEM_ATTRIBUTES("RampDownSetpoint",	"",	OpcUaType_Float,	3 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellSet,RESPONSE_ITEM_ATTRIBUTES("StandbyVoltSetpoint","",OpcUaType_Float,	4 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellSet,RESPONSE_ITEM_ATTRIBUTES("ProtectDelaySetpoint","",OpcUaType_Float,	6 )));

		/***/	// Monitoring Thread
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("HVSetpoint",		"", OpcUaType_Float,	0 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("ThresholdSetpoint",	"",	OpcUaType_Float,	1 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("RampUpSetpoint",	"", OpcUaType_Float,	2 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("RampDownSetpoint",	"",	OpcUaType_Float,	3 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("StandbyVoltSetpoint","",OpcUaType_Float,	4 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("ProtectDelaySetpoint","",OpcUaType_Float,	6 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("Error",				"",	OpcUaType_Boolean,	0 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("GeneratorStatus",	"",	OpcUaType_Boolean,	1 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("AccumError",		"",	OpcUaType_Boolean,	2 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("CurrentOverload",	"",	OpcUaType_Boolean,	3 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("StandbyStatus",		"",	OpcUaType_Boolean,	4 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("RampDownStatus",	"",	OpcUaType_Boolean,	6 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("RampUpStatus",		"",	OpcUaType_Boolean,	7 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("VoltageValue",		"",	OpcUaType_Float,	7 )));
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rCellAll,RESPONSE_ITEM_ATTRIBUTES("CurrentValue",		"",	OpcUaType_Float,	8 )));

		//																							itemID,				nodeID, varType,			dataIndex
		/*Q.01*/
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rQ01,RESPONSE_ITEM_ATTRIBUTES("HVSetpoint",		    "", OpcUaType_Float,	0 )));
		/*Q.03*/
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rQ03,RESPONSE_ITEM_ATTRIBUTES("ThresholdSetpoint",		"", OpcUaType_Float,	0 )));
		/*Q.12*/
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rQ12,RESPONSE_ITEM_ATTRIBUTES("StandbyVoltSetpoint",	"",OpcUaType_Float,	    0 )));
		/*H.10*/
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rH10,RESPONSE_ITEM_ATTRIBUTES("RampUpSetpoint",		"", OpcUaType_Float,	0 )));
		/*H.11*/
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rH11,RESPONSE_ITEM_ATTRIBUTES("RampDownSetpoint",		"", OpcUaType_Float,	0 )));
		/*H.14*/
		respItems.insert(pair<RESPONSE_ID, RESPONSE_ITEM_ATTRIBUTES>(rH14,RESPONSE_ITEM_ATTRIBUTES("ProtectDelaySetpoint", 	"", OpcUaType_Int16,	0 )));
		break;
		default:
			break;
		}
	};
};
//}  // *JO*
 

//#define HVSys_CFG_FILE _T("HVSys_cfg.xml")
//#define LOG_FILE_MAX_SIZE 10000000
//#define HVSYS_LOG_FILE1 _T("HVSys_log1.txt")
//#define HVSYS_LOG_FILE2 _T("HVSys_log2.txt")
#define PORT_MIN 1
#define PORT_MAX 99																	
#define BRANCH_MIN 0
#define BRANCH_MAX 3																	
#define CELL_MIN 1	//numbering from 1 - see SC508 manual
#define CELL_MAX 105
																
#endif //	__HVSYSDEFINITIONS_H__
