#ifndef __COMOBJECT_H__
#define __COMOBJECT_H__

#include "HVSysDefinitions.h"
#include "ComPortObject.h"
// #include "NmBuildingAutomation.h" *JO*  connected in Framework


namespace AddressSpace
{
	class ComObject 
	{
		UA_DISABLE_COPY(ComObject);

	public:
		ComObject( ComPortObject *pcp, ItemsDefs *idefs) { m_pcp = pcp; m_idefs = idefs; }
		virtual ~ComObject(void) {};


		ComPortObject* getComPort() { return m_pcp; }
		void setComPort(ComPortObject* pcp) { m_pcp = pcp; }
		ItemsDefs * m_idefs;
//		NmBuildingAutomation* m_nm;    *JO* connected in Framework
	private:
		ComPortObject* m_pcp; // for communication
	};
}
#endif
