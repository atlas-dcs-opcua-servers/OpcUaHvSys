#ifndef COMPORTOBJECT_H_
#define COMPORTOBJECT_H_

// #include "uaobjecttypes.h"
// #include "opcua_basedatavariabletype.h"
// #include "uabase.h"
// #include "uastring.h"
// #include "statuscode.h"
// #include "uaarraytemplates.h"
 #include "srvtrace.h"
// #include "xsdclass.h"
#include <Configuration.hxx>
#include "ComMessage.h"
#include "HVSysDefinitions.h"
#include "ComDriver.h"
#include "uasemaphore.h"

// namespace AddressSpace
// {
	class ComPortObject
	{
		UA_DISABLE_COPY(ComPortObject);
	public:
	ComPortObject(const Configuration::PORT  &config,CComDriver *cd);
	~ComPortObject(void);
	//void processTransaction(ComTransaction *ct);
	//void updateCaches(ComTransaction *ct, UaStatus& result);
	void		setName(UaString sName) { m_sPortID = sName; };
	UaString	getName(void) { return m_sPortID; };
	UaStatus	iniPort();
	// functions for initializing values from config file. Values need to be checked if they are supported by driver.
	UaStatus	setCommBus(const string& sBus);	
	UaStatus	setControllerType(const string& scontrollerType);
	UaStatus	setBaudRate(const string& sbaudrate);
	UaStatus	setDataBits(const string& sdatabits);
	UaStatus	setStopBits(const string& sstopbits);
	UaStatus	setParity(const string& sparity);
	UaStatus	setReadIntervalTimeout(const string& sReadIntervalTimeout);
	UaStatus	setReadTotalTimeoutConstant(const string& sReadTotalTimeoutConstant);
	UaStatus	setReadTotalTimeoutMultiplier(const string& sReadTotalTimeoutMultiplier);
	UaStatus	setWriteTotalTimeoutConstant(const string& sWriteTotalTimeoutConstant);
	UaStatus	setWriteTotalTimeoutMultiplier(const string& sWriteTotalTimeoutMultiplier);
	// functions for updating values from hardware(driver). Values don't need to be checked.
	void		setControllerType(OpcUa_Byte controller){ m_bController = controller;};
	void		setBaudRate(OpcUa_Int32 baudrate) { m_iBaud = baudrate;};
	void		setDataBits(OpcUa_Byte databits) { m_bData = databits; };
	void		setStopBits(OpcUa_Byte stopbits) { m_bStop = stopbits; };
	void		setParity(OpcUa_Byte parity)     { m_bParity = parity; };
	void		setReadIntervalTimeout(OpcUa_UInt32 ReadIntervalTimeout) { m_uiRit = ReadIntervalTimeout; };
	void		setReadTotalTimeoutConstant(OpcUa_UInt32 ReadTotalTimeoutConstant) { m_uiRttc = ReadTotalTimeoutConstant; };
	void		setReadTotalTimeoutMultiplier(OpcUa_UInt32 ReadTotalTimeoutMultiplier) { m_uiRttm = ReadTotalTimeoutMultiplier; };
	void		setWriteTotalTimeoutConstant(OpcUa_UInt32 WriteTotalTimeoutConstant) { m_uiWttc = WriteTotalTimeoutConstant; };
	void		setWriteTotalTimeoutMultiplier(OpcUa_UInt32	 WriteTotalTimeoutMultiplier) { m_uiWttm = WriteTotalTimeoutMultiplier; };

	OpcUa_Int32		getBaudRate(void) { return m_iBaud; };
	OpcUa_Byte		getDataBits(void) { return m_bData; };
	OpcUa_Byte		getStopBits(void) { return m_bStop; };
	OpcUa_Byte		getParity(void)	  { return m_bParity; };
	OpcUa_Byte		getControllerType(void) { return m_bController; };
	OpcUa_Byte		getCommBus(void) { return m_bBus; };
	OpcUa_UInt32	getReadIntervalTimeout(void) { return m_uiRit; };
	OpcUa_UInt32	getReadTotalTimeoutConstant(void) { return m_uiRttc; };
	OpcUa_UInt32	getReadTotalTimeoutMultiplier(void) { return m_uiRttm; };
	OpcUa_UInt32	getWriteTotalTimeoutConstant(void) { return m_uiWttc; };
	OpcUa_UInt32	getWriteTotalTimeoutMultiplier(void) { return m_uiWttm; };

	UaStatus		terminate(void);
	int getPortHandle() { return m_hPortHandle; };

	CComDriver* getComIO() {   
		return m_pcd;
	}
	void postCom() {
		m_comSemaphore.post(1); 
	}
	OpcUa_StatusCode waitCom() { 
		return m_comSemaphore.wait();
	}
	OpcUa_StatusCode timedWaitCom(OpcUa_UInt32 msec) { 
		return m_comSemaphore.timedWait(msec);
	}

	private:
		OpcUa_Byte			m_bBus;
		UaString			m_sPortID;
		OpcUa_Byte			m_bController;
		OpcUa_Int32			m_iBaud;	//	unsigned long m_BaudRate;
		OpcUa_Byte			m_bData;	//	unsigned char m_DataBits;
		OpcUa_Byte			m_bStop;	// unsigned char m_StopBits;
		OpcUa_Byte			m_bParity;	// unsigned char m_Parity;
		OpcUa_UInt32		m_uiRit;	// unsigned long m_ReadIntervalTimeout;
		OpcUa_UInt32		m_uiRttc;	// unsigned long m_ReadTotalTimeoutConstant;
		OpcUa_UInt32		m_uiRttm;	// unsigned long m_ReadTotalTimeoutMultiplier;
		OpcUa_UInt32		m_uiWttc;	// unsigned long m_WriteTotalTimeoutConstant;
		OpcUa_UInt32		m_uiWttm;	// unsigned long m_WriteTotalTimeoutMultiplier;
		OpcUa_Boolean		m_stop;
		CComDriver*			m_pcd;
	protected:
		UaSemaphore			m_comSemaphore;
		int					m_hPortHandle;
	};

// }
#endif /*COMPORTOBJECT_H_ */
