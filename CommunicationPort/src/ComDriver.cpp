// ComDriver.cpp: implementation of the CComDriver class.
//
//////////////////////////////////////////////////////////////////////


#include "ComDriver.h"
#include "HVSysDefinitions.h"
#include "ComMessage.h"


UaStatus CComDriver::iniPort(string name, unsigned long BaudRate, unsigned char DataBits, unsigned char Parity, unsigned char StopBits, 
					 unsigned long ReadIntervalTimeout, unsigned long ReadTotalTimeoutConstant, unsigned long ReadTotalTimeoutMultiplier, 
					 unsigned long WriteTotalTimeoutConstant, unsigned long WriteTotalTimeoutMultiplier, int *pfd)
{
	// name is defined in cofig file as ttyPS0, ttyPS1, etc
	int fd;
	UaStatus result = OpcUa_Good;
	string device = "/dev/";
	struct termios options;

	device += name;
	if(*pfd>0) close(*pfd); // make sure port is closed
	fd=open(device.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);  
	// access mode: read/write | creation flag: other proccesses can't control port | no blocking I/O (DCD line)
	if( fd <= 0) 
	{
		result = OpcUa_BadResourceUnavailable;
		LOG(Log::ERR) << "Device " << device.c_str() << " failed to open port: " << errno;
				*pfd = (int)NULL;
	}
	else
	{
		LOG(Log::INF) << "Device " << device.c_str() << " port opened, descriptor: " << fd;
				*pfd = fd;
		// blocking port for timeout while waiting for read
		if(fcntl(fd, F_SETFL, 0)<0)
		{
			result = OpcUa_BadDeviceFailure;
			LOG(Log::ERR) << "Can't set blocking device " << device.c_str() << ": " << errno;
			return result;
		}

		// get device attributes
		if(tcgetattr(fd, &options)<0)
		{
			result = OpcUa_BadDeviceFailure;
			LOG(Log::ERR) << "Can't get device " << device.c_str() << " attributes: " << errno;
			return result;
		}

		options.c_cflag &= ~CRTSCTS;  // disable hardware flow control
		options.c_cflag |= (CLOCAL | CREAD);  // enable receiver and local mode
		options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);  // raw input
		options.c_iflag &= ~(ICRNL | INLCR); // don't map CR to NL or NL to CR
		options.c_iflag &= ~IUCLC; // don't map uppercase to lowercase
		options.c_iflag &= ~IGNCR; // don't ignore CR
		options.c_iflag |= IGNBRK;  // ignore BREAK condition
		options.c_iflag &= ~(IXON | IXOFF | IXANY); // disable software flow control
		options.c_oflag &= ~OPOST;  // raw output - other c_oflag bits ignored

		// flush in and out buffer and set new settings than
		if(tcsetattr(fd, TCSAFLUSH, &options)<0)
		{
			result = OpcUa_BadDeviceFailure;
			LOG(Log::ERR) << "Can't set device " << device.c_str() << " attributes: " << errno;
			return result;
		}

		result = setCOMstate(fd, BaudRate, DataBits, Parity, StopBits);
		if(result.isGood())
		{
			result = setCOMTimeouts(fd, ReadIntervalTimeout, ReadTotalTimeoutConstant, ReadTotalTimeoutMultiplier, 
										WriteTotalTimeoutConstant, WriteTotalTimeoutMultiplier);
		}
		if(result.isGood())
		{
			LOG(Log::INF) << "Device " << device.c_str() << " initialized, descriptor: " << fd;
		}
	}
	return result;
}

speed_t CComDriver::getSpeed(unsigned long BaudRate) 
{
	speed_t sret; 
	
	switch (BaudRate) {
	case 9600:
		sret = B9600;
		break;
	case 19200: 
		sret = B19200;
		break;
	case 38400: 
		sret = B38400;
		break;
	case 57600: 
		sret = B57600;
		break;
	case 115200: 
		sret = B115200;
		break;
	default: 
		sret = B9600;
		break;
	}
	return(sret);
}

bool CComDriver::isSupportedParameter(char *sValue,int nParam)
{
int iValue; 
bool bret = false;
string value = sValue;
	
switch(nParam)
{
case BAUD_RATE:
	sscanf(value.c_str(), "%d", &iValue);
	switch (iValue) {
		case 9600:
		case 19200:
		case 38400:
		case 57600:
		case 115200:
			bret = true;
			break;
		}
	break;
case COMM_BUS:
	if((value.compare("USB") == 0) || (value.compare("RS232") ==0))
		bret = true;
	break;
case DATA_BITS:
	sscanf(value.c_str(), "%d", &iValue);
	switch (iValue) {
		case 8:
			bret = true;
			break;
		}
	break;
case STOP_BITS:
	if(value.compare("1") == 0) bret = true;
	break;
case PARITY:
	if(value.compare("No") == 0) bret = true;
	break;
case CONTROLLER_TYPE:
	if(value.compare("SC508") ==0 )
		bret = true;
	break;
case READINTERVALTIMEOUT:
case READTOTALTIMEOUTCONSTANT:
case READTOTALTIMEOUTMULTIPLIER:
case WRITETOTALTIMEOUTCONSTANT:
case WRITETOTALTIMEOUTMULTIPLIER:
	sscanf(value.c_str(), "%d", &iValue);
	if((iValue>=0) && (iValue<=1000)) bret = true;
	break;
}

return(bret);
}

UaStatus CComDriver::setCOMstate(int fd, unsigned long BaudRate, unsigned char DataBits, unsigned char Parity, unsigned char StopBits)
{
	struct termios options;
	std::string sParity, sStopBits;
	UaStatus result = OpcUa_Good;
	
	if(tcgetattr(fd, &options)<0)
	{
		result = OpcUa_BadDeviceFailure;
		LOG(Log::ERR) << " Can't get device " << fd << " attributes: " << errno;
		return getPortError(fd);
	}

	// setting in and out speed
	if(cfsetispeed(&options, getSpeed(BaudRate))<0)
	{
		result = OpcUa_BadDeviceFailure;
		LOG(Log::ERR) << " Can't set device " << fd << " input speed: " << errno;
		return result;
	}
	if(cfsetospeed(&options, getSpeed(BaudRate))<0)
	{
		result = OpcUa_BadDeviceFailure;
		LOG(Log::ERR) << " Can't set device " << fd << " output speed: " << errno;
		return result;
	}
	// setting frame attributes
	switch(Parity) {
	case NOPARITY:
		options.c_cflag &= ~PARENB;
		options.c_iflag &= ~(INPCK | ISTRIP);  // disable parity checking
		sParity = "No";
		break;
	case ODDPARITY:
		options.c_cflag |= PARENB;
		options.c_cflag |= PARODD;
		options.c_iflag |= INPCK;  // enable parity checking
		options.c_iflag |= PARMRK;  // mark parity errors
		sParity = "Odd";
		break;
	case EVENPARITY:
		options.c_cflag |= PARENB;
		options.c_cflag &= ~PARODD;
		options.c_iflag |= INPCK;  // enable parity checking
		options.c_iflag |= PARMRK;  // mark parity errors
		sParity = "Even";
		break;
	default:
		options.c_cflag &= ~PARENB;
		options.c_iflag &= ~(INPCK | ISTRIP);  // disable parity checking
		sParity = "No";
		break;
		}
	switch(DataBits) {
	case 5:
		options.c_cflag &= ~CSIZE; // clears the curent setting
		options.c_cflag |= CS5;
		break;
	case 6:
		options.c_cflag &= ~CSIZE; // clears the curent setting
		options.c_cflag |= CS6;
		break;
	case 7:
		options.c_cflag &= ~CSIZE; // clears the curent setting
		options.c_cflag |= CS7;
		break;
	case 8:
		options.c_cflag &= ~CSIZE; // clears the curent setting
		options.c_cflag |= CS8;
		break;
	default:
		options.c_cflag &= ~CSIZE; // clears the curent setting
		options.c_cflag |= CS8;
		break;
	}
	switch(StopBits) {
	case ONESTOPBIT:
		options.c_cflag &= ~CSTOPB;
		sStopBits = "1";
		break;
	case TWOSTOPBITS:
		options.c_cflag |= CSTOPB;
		sStopBits = "2";
		break;
	default:
		options.c_cflag &= ~CSTOPB;
		break;
		sStopBits = "1";
	}
	// flush in and out buffer and set new settings than
	if(tcsetattr(fd, TCSAFLUSH, &options)<0)
	{
		result = OpcUa_BadDeviceFailure;
		LOG(Log::ERR) << " Can't set device " << fd << " attributes: " << errno;
		return result;
	}
	LOG(Log::INF) << " Port " << fd << " set to: BaudRate = " << BaudRate << ", DataBits = " << DataBits << ", Parity = " << sParity.c_str() << ", StopBits = " << sStopBits.c_str();
	return result;
}

UaStatus CComDriver::setCOMTimeouts(int fd, unsigned long ReadIntervalTimeout, unsigned long ReadTotalTimeoutConstant, 
									unsigned long ReadTotalTimeoutMultiplier, unsigned long WriteTotalTimeoutConstant, 
									unsigned long WriteTotalTimeoutMultiplier)
{
	UaStatus result = OpcUa_Good;	
	struct termios options;

	if(tcgetattr(fd, &options)<0)
	{
		result = OpcUa_BadDeviceFailure;
		LOG(Log::ERR) << " Can't get device " << fd << " attributes: " << errno;
		return result;
	}

	// setting timeouts
	options.c_cc[VMIN] = 0; // minimum amount of characters to be read
	options.c_cc[VTIME] = (int)(ReadTotalTimeoutConstant/100) + 1; // read timeout in tenths of second

// flush in and out buffer and set new settings than
	if(tcsetattr(fd, TCSAFLUSH, &options)<0)
	{
		result = OpcUa_BadDeviceFailure;
		LOG(Log::ERR) << " Can't set device " << fd << " attributes: " << errno;
		return result;
	}
	LOG(Log::INF) << " Port " << fd << " timeout set to " << (int)(ReadTotalTimeoutConstant/100 + 1) << " x 0.1 sec.";
	return OpcUa_Good;
}

int CComDriver::getCOMParameter(const string& sValue,int nParam)
{
int iret = -1, iValue;
string value = sValue;
	
switch(nParam)
{
case BAUD_RATE:
	sscanf(value.c_str(), "%d", &iValue);
	switch (iValue) {
		case 9600:
		case 19200:
		case 38400:
		case 57600:
		case 115200:
			iret = iValue;
			break;
		}
	break;
case DATA_BITS:
	sscanf(value.c_str(), "%d", &iValue);
	switch (iValue) {
		case 8:
			iret = iValue;
			break;
		}
	break;
case STOP_BITS:
	if(value.compare("1") == 0) iret = ONESTOPBIT;
	if(value.compare("2") == 0) iret = TWOSTOPBITS;
	break;
case PARITY:
	if(value.compare("Even") == 0)  iret = EVENPARITY;
	if(value.compare("Odd") == 0)   iret = ODDPARITY;
	if(value.compare("No") == 0)    iret = NOPARITY;
	break;
case CONTROLLER_TYPE:
	if(value.compare("SC508") == 0)
		iret = TYPE_SC508;
	break;
case READINTERVALTIMEOUT:
case READTOTALTIMEOUTCONSTANT:
case READTOTALTIMEOUTMULTIPLIER:
case WRITETOTALTIMEOUTCONSTANT:
case WRITETOTALTIMEOUTMULTIPLIER:
	sscanf(value.c_str(), "%d", &iValue);
	if((iValue>=0) && (iValue<=1000))iret = iValue;
	break;
}

return(iret);
}


UaStatus CComDriver::getPortError(int fd)
{
	LOG(Log::ERR) << " device " << fd << "  last error:  " << errno;
	return OpcUa_BadDeviceFailure;
}

void CComDriver::flushCOMPort(int fd, IO_DIR dir)
{
	switch(dir) {
	case IO_IN:
		tcflush(fd, TCIFLUSH);
		break;
	case IO_OUT:
		tcflush(fd, TCOFLUSH);
		break;
	case IO_INOUT:
		tcflush(fd, TCIOFLUSH);
		break;
	default:
		tcflush(fd, TCIOFLUSH);
		break;
	}
}

void CComDriver::sendMessage(int fd, ComMessage *mess)
{
	short retry =0;
	int TxWritten;
	bool done = false;
	
	tcflush(fd,TCIOFLUSH);
	LOG(Log::TRC) << "sendMessage - " << names[fd].c_str() << " Tx: , " << mess->getTxDataByte(0) << ", " << mess->getTxDataByte(1);
	LOG(Log::TRC) << mess->getTxDataByte(2) << mess->getTxDataByte(3) << mess->getTxDataByte(4) << mess->getTxDataByte(5) << mess->getTxDataByte(6) << mess->getTxDataByte(7);
	while((!done)  && (retry < MAX_COM_WRITE_RETRY))			  
	{
		if((TxWritten = write(fd,&(mess->m_TxData),mess->getTxBytes())) == (int)mess->getTxBytes())
		{
			done=true;
			mess->setTxResult(OpcUa_Good);
		}
		else
		{
			if (TxWritten == -1) mess->setTxResult(getPortError(fd)); // get last error
			retry++;
		}
		if(retry==1)
		{
			LOG(Log::TRC) << "sendMessage failed: " << names[fd].c_str() << " "<< TxWritten << "(of " << mess->getTxBytes() << ") Bytes, result=" << mess->getTxResult().statusCode() << ", retry= " << retry;
		}
		else
		{
			LOG(Log::INF) << "sendMessage failed: " << names[fd].c_str() << " "<< TxWritten << "(of " << mess->getTxBytes() << ") Bytes, result=" << mess->getTxResult().statusCode() << ", retry= " << retry;
		}

	}


	if ((retry == MAX_COM_WRITE_RETRY) || (TxWritten != (int)mess->getTxBytes()) ) 
	{
		if(TxWritten == -1) mess->setTxResult(getPortError(fd)); // get last error
		if( (mess->getTxResult().isGood())  && (TxWritten != (int)mess->getTxBytes()) )
		{
			mess->setTxResult(OpcUa_BadCommunicationError);
			HvSysCommPurge(fd, true, mess);
		}
	}
	return;
}

unsigned char CComDriver::sendRespMessage(int fd, ComMessage *mess, short errCodeType)
{
	short write_retry, read_retry, req_retry;
	int TxWritten,RxRead;
	unsigned int i,j;
	bool done;
	unsigned char errCode = 0;
	unsigned char command = mess->getTxDataByte(0);
	
	req_retry =0;
	j = 0;
	done = false;
	tcflush(fd,TCIOFLUSH);
	LOG(Log::TRC) << "sendRespMessage - " << names[fd].c_str() << " Tx:, " << mess->getTxDataByte(0) << ", " << mess->getTxDataByte(1);
	LOG(Log::TRC) << mess->getTxDataByte(2) << mess->getTxDataByte(3) << mess->getTxDataByte(4) << mess->getTxDataByte(5) << mess->getTxDataByte(6) << mess->getTxDataByte(7);
	

	while((!done)  && (req_retry < MAX_REQ_RETRY))
	{
		write_retry = 0;
		read_retry = 0;

//									 SEND request message LOOP								//
		while((!done)  && (write_retry < MAX_COM_WRITE_RETRY))			  
		{
			if((TxWritten = write(fd,&(mess->m_TxData),mess->getTxBytes())) == (int)mess->getTxBytes())  // request sent properly
			{
				done=true;
				mess->setTxResult(OpcUa_Good);
				LOG(Log::TRC) << "sendRespMessage - " << names[fd].c_str() << " Tx(OK): (" << TxWritten << "(of " << mess->getTxBytes() <<  ") Bytes, write_retry=" << write_retry;
			}
			else  // send request failed, retry write
			{
				if(TxWritten == -1) mess->setTxResult(getPortError(fd)); //get last error				
				if((write_retry==0) && (req_retry == 0))
				{
			    LOG(Log::TRC) << "sendRespMessage - " << names[fd].c_str() << " Tx(Failed): (" << TxWritten << "(of " << mess->getTxBytes() <<  ") Bytes, result=(" << mess->getTxResult().statusCode() << "), write_retry=" << write_retry;
				}
				else
				{
				LOG(Log::INF) << "sendRespMessage - " << names[fd].c_str() << " Tx(Failed): (" << TxWritten << "(of " << mess->getTxBytes() <<  ") Bytes, result=(" << mess->getTxResult().statusCode() << "), write_retry=" << write_retry;
				}
				write_retry++;
			}
		}
		//									 SEND request message LOOP ENDED							//



		if ((write_retry == MAX_COM_WRITE_RETRY) || (TxWritten != (int)mess->getTxBytes()) ) // multiple send request attempts failed
		{
			if(TxWritten == -1) mess->setTxResult(getPortError(fd)); // get last error
			if( (mess->getTxResult().isGood())  && (TxWritten != (int)mess->getTxBytes()) )
			{
				mess->setTxResult(OpcUa_BadCommunicationError);
				HvSysCommPurge(fd, true, mess); //true
			}
			read_retry = MAX_COM_READ_RETRY; // nothing to read when write failed
		}
		else   // send request suceeded
		{
			mess->setTxResult(OpcUa_Good);
			read_retry = 0;
			i=0;
			j=mess->getRxBytes();
			done=false;
			usleep(READ_RESPONSE_SLEEP_TIME*1000);
		}

//									 READ response message LOOP							//
		LOG(Log::TRC) << "sendRespMessage - " << names[fd].c_str() << "Rx";
		while(!done && read_retry < MAX_COM_READ_RETRY)
		{
			if( (RxRead = read(fd,&(mess->m_RxData[i]),j)) == (int)j )
			// response read as expected
			{
				done = true;
				i += RxRead;  // should be full message lenght
				j -= RxRead;  // should be 0
				mess->setRxResult(OpcUa_Good);
				LOG(Log::TRC) << "(OK): " << RxRead << "(of " << j << ") Bytes, read_retry=" << read_retry;
			} 

			else // read response failed 
			{
				if(RxRead == -1) mess->setRxResult(getPortError(fd)); //read failed, get last error
				else {           // only part of message is read - delayed response from Crate
					i += RxRead; //points to first "not received" byte in RxData
					j -= RxRead; //how many bytes is missed
				}
				
				if((read_retry == 0) && (req_retry == 0))
				{
					LOG(Log::TRC) << "sendRespMessage - " << names[fd].c_str() << " Rx(Failed): (" << i << "(of " << mess->getRxBytes() <<  ") Bytes, result=(" << mess->getTxResult().statusCode() << "), read_retry=" << read_retry;

				}
				else
				{
					LOG(Log::INF) << "sendRespMessage - " << names[fd].c_str() << " Rx(Failed): (" << i << "(of " << mess->getRxBytes() <<  ") Bytes, result=(" << mess->getTxResult().statusCode() << "), read_retry=" << read_retry;
				}
			}

            // internal Crate Errors handling
			if((errCodeType==0) && ((int)i>=1))   // decimal error coding (type=0, errCode in RxData[0])
			{
				errCode = mess->m_RxData[0];
			}
			if(errCodeType==1)   // ASCII error coding (type=1, ASCII errCode in RxData[3])
			{
				errCode = 0;
				if((int)i==6)    // ASCII error message is 6 bytes long
				{
					char cstr[5];
					for(int itr = 0; itr<=3; itr++) cstr[itr] = mess->m_RxData[itr];
					cstr[4] = '\0';
					sscanf(cstr," Er%hhu",&errCode);
				}
			}

			if(errCodeType>=0)   
			{
				if((errCode > 9) || (errCode == 1)) 
					// not a valid error code or no response from Cell, retry whole request
				{
					read_retry = MAX_COM_READ_RETRY-1;
				}
				else if(errCode != 0)
				{					
					tcflush(fd, TCIFLUSH);
					LOG(Log::ERR) << "sendRespMessage (errCode = " << errCode << "), flush port input";
					done = true;
				}
				LOG(Log::TRC) << "sendRespMessage" << names[fd].c_str() << " read error code: " << errCode << ", req_retry=" << req_retry << ", read_retry=" << read_retry << ", done" << done;
			}
            // internal Crate Errors handling

			if(!done) // retry read response
			{
				read_retry++;
				LOG(Log::TRC) << "sendRespMessage" << names[fd].c_str() << " read retry incremented: read_retry="<< read_retry << ", req_retry=" << req_retry << ", done" << done;
				usleep(READ_COM_RETRY_SLEEP_TIME * 1000);

				switch(command)
				{
				case '#':
					usleep(2500 * 1000);  // additional delay for Low Voltage switch ON confirmation
					break;
				case '_':
					usleep(250 * 1000);  // additional delay for Low Voltage switch OFF confirmation
					break;
				default:
					break;
				}
			}
		}
//									 READ response message LOOP	ENDED						//		

		if(!done) 
		{
			mess->setRxResult(OpcUa_BadRequestTimeout);
			HvSysCommPurge(fd, true, mess); //false
			req_retry++;
			LOG(Log::TRC) << "sendRespMessage" << names[fd].c_str() << " I/O Request failed " << req_retry << " times (" << mess->m_TxData[0] << "," << mess->m_TxData[1] << ")";
			usleep(REQ_RETRY_SLEEP_TIME*1000);
		}
	}
	LOG(Log::TRC) << "sendRespMessage - " << names[fd].c_str() << " Rx buffer: (" << mess->getRxDataByte(0) << "), (" << mess->getRxDataByte(1) << "), (" << mess->getRxDataByte(2) << "), (" << mess->getRxDataByte(3) << ")";
	LOG(Log::TRC) <<"(" << mess->getRxDataByte(4) << "), (" << mess->getRxDataByte(5) << "), (" << mess->getRxDataByte(6) << "), (" << mess->getRxDataByte(7) << ")result=(" << mess->getRxResult().statusCode() << "), read_retry= " << read_retry;
	if((read_retry!=0) || (req_retry!=0))
	{
		LOG(Log::TRC) << "sendRespMessage - " << names[fd].c_str() << ": req_retry " << req_retry << ", read_retry " << read_retry << ", done " << done << ", errCodeType " << errCodeType << ", errCode " << errCode;
	}
	return errCode;
}

unsigned char  CComDriver::getMessage(int fd,ComMessage *mess, short errCodeType)
{
	short retry;
	int RxRead;
	unsigned int i,j;
	bool done;
	unsigned char errCode = 0;
	
	retry =0;
	i=0;
	j=mess->getRxBytes();
	done=false;

	LOG(Log::TRC) << "getMessage - " << names[fd].c_str() << " Rx";
	while(!done && retry < MAX_COM_READ_RETRY)
	{
		if( (RxRead = read(fd,&(mess->m_RxData[i]),j)) == (int)j )
		{
			done = true;
			mess->setRxResult(OpcUa_Good);
			LOG(Log::TRC) << "(OK): " << RxRead << "(of " << j << " Bytes, retry=" << retry;
		}
		else
		{
			if(RxRead == -1) mess->setRxResult(getPortError(fd)); //read failed, get last error
			else {           // only part of message is read - delayed response from Crate
					i += RxRead; //points to first "not received" byte in RxData
					j -= RxRead; //how many bytes is missed
				}
			if(retry<2)
			{	
				LOG(Log::TRC) << "(Failed): " << i << "(of " << mess->getRxBytes() << " Bytes, result=" << mess->getRxResult().statusCode()<< " , retry=" << retry;
			}
			else
			{
				LOG(Log::INF) << "(Failed): " << i << "(of " << mess->getRxBytes() << " Bytes, result=" << mess->getRxResult().statusCode()<< " , retry=" << retry;
			}

            // internal Crate Errors handling
			if((errCodeType==0) && ((int)i>=1))   // decimal error coding (type=0, errCode in RxData[0])
			{
				errCode = mess->m_RxData[0];
			}
			if((errCodeType==1) && ((int)i==6))   // ASCII error coding (type=1, ASCII errCode in RxData[3])
			{
				char cstr[5];
				for(int itr = 0; itr<=3; itr++) cstr[itr] = mess->m_RxData[itr];
				cstr[4] = '\0';
				sscanf(cstr," Er%hhu",&errCode);
			}
			if((errCodeType>=0) && (errCode != 0)) done = true;
            // internal Crate Errors handling
			if(!done)
			{
				retry++;
				usleep(READ_COM_RETRY_SLEEP_TIME*1000);
			}
		}
	}
	if(!done) 
	{
		mess->setRxResult(OpcUa_BadRequestTimeout);
		HvSysCommPurge(fd, false, (ComMessage*)NULL);
	}

	/*LOG(Log::TRC) << "getMessage - " << names[fd].c_str() << ": (" << mess->getRxDataByte(0) << "), ("
				  << mess->getRxDataByte(1) << "), (" << mess->getRxDataByte(2) << "), ("
				  << mess->getRxDataByte(3) << ")" << "(" << mess->getRxDataByte(4) << "), ("
				  << mess->getRxDataByte(5) << "), (" << mess->getRxDataByte(6) << "), ("
				  << mess->getRxDataByte(7) << ")";*/
	return errCode;
}

void CComDriver::HvSysCommPurge(int fd, bool purgeHV_COM, ComMessage *mess)
{
	int TxWritten, RxRead;

	//char buff[8] = {'M','M','M','M','M','M','M','M'};
	char buff[16] = {0x0D, 0x0A, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	int i;


	tcflush(fd,TCIOFLUSH);
	LOG(Log::TRC) << "HvSysCommPurge - " << names[fd].c_str() << ": Port Tx and Rx buffers purged, COM errors cleared.";
	if(purgeHV_COM) 
	{
		if((mess->m_TxData[0] == 'H') || (mess->m_TxData[0] == 'Q'))
		{
			buff[0] = 'H';
			buff[1] = 10;
			buff[2] = mess->getTxDataByte(2);
			buff[3] = mess->getTxDataByte(3);
			TxWritten = write(fd,buff,4);
		}
		else
		{
			buff[0] = 'M';
			TxWritten = write(fd,buff,1);
		}
		LOG(Log::TRC) << "HvSysCommPurge " << names[fd].c_str() << "send to HVSys " << TxWritten << " Bytes written";
		usleep(100);
		for(i=0; i<16; i++) buff[i] = 0x0;
		RxRead = read(fd, &buff, 16);
		LOG(Log::TRC) << "HvSysCommPurge " << names[fd].c_str() << " Buff read(" << RxRead << " of 16)";
	}
	return;
}
UaStatus CComDriver::terminatePort(int fd)
{
	UaStatus result;

	if(close(fd) == -1) result = OpcUa_BadDeviceFailure;
	else result =  OpcUa_Good;
	return result;
}

void CComDriver::addToPortsMaps( int fd, string name)
{
	map<int, string>::iterator nit;
	names.insert(map<int, string>::value_type(fd, name));
	return;
}
CComDriver::CComDriver() {}
CComDriver::~CComDriver(void) {}
