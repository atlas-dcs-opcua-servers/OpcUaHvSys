#include "ComTransaction.h"
#include "LogIt.h"



	ComTransaction::ComTransaction(ComPortObject *pcp): m_semaphore(0,1)
	{
		m_pcp = pcp;
		m_simple = true;
		m_Slope = 1.0;
		m_bRead = false;
		m_uint32 = 0;
		m_responseID = rNo;
		m_WCommand = ' ';
		m_RCommand = ' ';
		m_CellSubAddr = 0;
		m_CellID = -1;
		m_BranchID = -1;
		m_Offset = 0;
		m_dataIndex = 0;
		m_sValue = 0;
		m_WfunctionID = fNo;
		m_RfunctionID = fNo;
		m_itemType = OpcUaType_String;
		m_CalibType = noCalib;
		m_ubool = OpcUa_False;
		m_timestamp = time(NULL);

	}

	ComTransaction::ComTransaction(const unsigned char *cb, const unsigned char *cc, std::vector<HW_ITEM_ATTRIBUTES>::iterator ia, ComPortObject *pcp): m_semaphore(0,1)
	{
		m_pcp = pcp;
		m_itemType = ia->varType();
		m_dataIndex = ia->dataIndex();
		m_initResult = OpcUa_BadNotImplemented;
		m_ubool = OpcUa_False;
		m_uint32 = 0;
		m_bRead = false;
		m_simple = true;
		m_sValue = 0;
		m_responseID = rNo;
		m_timestamp = time(NULL);
		m_BranchID = *cb;
		m_CellID = *cc;
		m_PortID = pcp->getName();
		LOG(Log::TRC) << "Transaction for " << m_PortID.toUtf8() << ".Branch" << (int)m_BranchID << ".Cell"<< (int)m_CellID;

		if(*cc == 255)
		{
			m_Slope = 1.0;
			m_Offset = 0.0;
		}

		m_CalibType = ia->calibrationType();   //calibration to be done

		string opString = ia->readCommand().c_str();
		if(opString.compare("")==0)
		{
			m_RfunctionID = fNo;
			m_RCommand = ' ';
			m_initResult = OpcUa_Good;
		}
		if(opString.compare("P")==0)
		{
			m_RfunctionID = fP;
			m_RCommand = 'P';
			m_initResult = OpcUa_Good;
		}
		if(opString.compare("3")==0)
		{
			m_RfunctionID = f3;
			m_RCommand = '3';
			m_initResult = OpcUa_Good;
		}
		if(opString.compare("M")==0)
		{
			m_RfunctionID = fM;
			m_RCommand = 'M';
			m_initResult = OpcUa_Good;
		}
		if(opString.compare(0,2,"Q.")==0)
		{
			m_RfunctionID = fQ;
			m_RCommand = 'Q';
			opString.replace(0,2,"");
			sscanf(opString.c_str(), "%hhu", &m_CellSubAddr);
			m_initResult = OpcUa_Good;
		}
		if(opString.compare(0,2,"H.")==0)
		{
			m_RfunctionID = fH;
			m_RCommand = 'H';
			opString.replace(0,2,"");
			sscanf(opString.c_str(), "%hhu", &m_CellSubAddr);
			m_initResult = OpcUa_Good;
		}
		if(opString.compare("LastValue")==0)
		{
			m_RfunctionID = fLastValue;
			m_RCommand = ' ';
			m_initResult = OpcUa_Good;
		}
		if(opString.compare("BusAttr")==0)
		{
			m_RfunctionID = fBusAttr;
			m_RCommand = ' ';
			m_initResult = OpcUa_Good;
		}
		if(opString.compare("PortAttr")==0)
		{
			m_RfunctionID = fPortAttr;
			m_RCommand = ' ';
			m_initResult = OpcUa_Good;
		}
		if(opString.compare("TraceAttr")==0)
		{
			m_RfunctionID = fTraceAttr;
			m_RCommand = ' ';
			m_initResult = OpcUa_Good;
		}
		if(m_initResult.isNotGood()) { LOG(Log::ERR) << opString.c_str() << " read operation not implemented";}

		opString = ia->writeCommand().c_str();
		if(opString.compare("")==0)
		{
			m_WfunctionID = fNo;
			m_RCommand = ' ';
			m_initResult = OpcUa_Good;
		}
		if(opString.compare("EO")==0)
		{
			m_WfunctionID = fBVswitch;
			m_initResult = OpcUa_Good;
		}
		if(opString.compare("_#")==0)
		{
			m_WfunctionID = fLVswitch;
			m_initResult = OpcUa_Good;
		}
		if(opString.compare("B")==0)
		{
			m_WfunctionID = fB;
			m_initResult = OpcUa_Good;
		}
		if(opString.compare(0,2,"Z.")==0)
		{
			m_WfunctionID = fZ;
			m_WCommand = 'Z';
			opString.replace(0,2,"");
			sscanf(opString.c_str(), "%hhu", &m_CellSubAddr);
			m_initResult = OpcUa_Good;
		}
		if(opString.compare("PortAttr")==0)
		{
			m_WfunctionID = fPortAttr;
			m_WCommand = ' ';
			m_initResult = OpcUa_Good;
		}
		if(opString.compare("TraceAttr")==0)
		{
			m_WfunctionID = fTraceAttr;
			m_WCommand = ' ';
			m_initResult = OpcUa_Good;
		}
		if(m_initResult.isNotGood()) { LOG(Log::ERR) << opString.c_str() << " write operation not implemented"; }
		LOG(Log::TRC) << "Branch: " << m_BranchID << "Cell: "<< m_CellID;
	}

	ComTransaction::~ComTransaction()
	{
	}

	void ComTransaction::setCalib(float setSlope, float setOffset, float monSlope, float monOffset)
	{
		switch(m_CalibType)
		{
		case setLinear:
			m_Slope = setSlope;
			m_Offset = setOffset;
			break;
		case monLinear:
			m_Slope = monSlope;
			m_Offset = monOffset;
			break;
		case noCalib:
		default:
			m_Slope = 1.0;
			m_Offset = 0.0;
			break;
		}
	}

	void ComTransaction::initMonTransaction(FUNCTION_ID fId)
	{
		m_simple 		= true;
		m_bRead 		= true; // read = true; write = false;
		m_RfunctionID 	= fId;

		switch(fId){
		case fP:
			m_RCommand = 'P';
			m_initResult = OpcUa_Good;
			m_CalibType = noCalib;
			break;
		case fM:
			m_RCommand = 'M';
			m_initResult = OpcUa_Good;
			m_CalibType = noCalib;
			break;
		case f3:
			m_RCommand = '3';
			m_initResult = OpcUa_Good;
			m_CalibType = noCalib;
			break;
		case fCellMon:
			m_RCommand = 'c';
			m_initResult = OpcUa_Good;
			m_CalibType = monLinear;
			break;
		default:
			m_CalibType = noCalib;
			m_initResult = OpcUa_BadNotImplemented;
			break;
		}
		setResponseID();
	}

	void ComTransaction::initMonTransaction(unsigned char cb, unsigned char cc,FUNCTION_ID fId)
	{
		m_CellID 		= cc;
		m_BranchID 		= cb;
		initMonTransaction(fId);
	}
		
	void ComTransaction::initComTransaction(UaVariant *var, bool direction)
	{
		m_simple = true;      // complex will be set in some cases below 
		m_bRead = direction; // read = true; write = false;
		if(direction == false) 
		{
			switch(m_WfunctionID) {
			case fBVswitch:
				{
					OpcUa_Boolean data;
					m_initResult = var->changeType(OpcUaType_Boolean, OpcUa_False);
					var->toBool(data);
					if(data != OpcUa_False) 
					{
						m_bValues[0] = true;
						m_WCommand = 'E';
					}
					else
					{
						m_bValues[0] = false;
						m_WCommand = 'O';
					}
				}
				break;

			case fLVswitch:
				{
					OpcUa_Boolean data;
					m_initResult = var->changeType(OpcUaType_Boolean, OpcUa_False);
					var->toBool(data);
					if(data != OpcUa_False)
					{
						m_bValues[0] = true;
						m_WCommand = '#';
					}
					else
					{
						m_bValues[0] = false;
						m_WCommand = '_';
					}
				}
				break;

			case fB:
				{
					UaString uStr(var->toString());
					m_WCommand = 'B';
					if(uStr == UaString("USB"))
					{
						m_bValues[0] = false;
						m_initResult = OpcUa_Good;
					}
					if(uStr == UaString("RS232"))
					{
						m_bValues[0] = true;
						m_initResult = OpcUa_Good;
					}
					if((uStr != UaString("RS232")) && (uStr != UaString("USB")))
					{
						m_initResult = OpcUa_BadNotSupported;
						LOG(Log::ERR) << uStr.toUtf8() << " bus not supported";
					}
				}
				break;

			case fZ:
				{
					switch(m_itemType)
					{
					case OpcUaType_Int16:
						m_initResult = var->changeType(OpcUaType_Int16, OpcUa_False);
						var->toInt16(m_sValue);
						//printf("integer %d\n", m_sValue);
						break;
					case OpcUaType_Float:
						m_initResult = var->changeType(OpcUaType_Float, OpcUa_False);
						if(m_initResult.isGood())
						{
							var->toFloat(m_fValues[0]);
						}
						else   // conversion from negative double values to floats doesn't work and gives OpcUa_OutOfRange error code !!!!
						{
							double dval;
							var->toDouble(dval);
							m_fValues[0] = (float)dval;
							m_initResult = OpcUa_Good;
						}
						break;
					case OpcUaType_Boolean:
						m_initResult = var->changeType(OpcUaType_Boolean, OpcUa_False);
						var->toBool((OpcUa_Boolean &)m_bValues[0]);
						break;
					default:
						m_initResult = OpcUa_BadNotSupported;
						LOG(Log::ERR) << "itemType not supported";
						break;
					}
					
					switch(m_CellSubAddr)
					{
					case 1:       // HVSetpoint
					case 3:       // ThresholdSetpoint
					case 12:      // StandbySetpoint
						m_simple  =  false;
						break;
					default:
						m_simple = true;
						break;
					}
				}
				break;
			default:
				break;
			}
		}

		setResponseID();
	}

	UaStatus ComTransaction::processComTransaction(UaVariant *confVar)
	{
		FUNCTION_ID		fID;
		UaStatus		result = OpcUa_Good;
		ComMessage		CMess;
		unsigned char	Command;
		unsigned short	writeWord, readWord;
		unsigned char	writeByte;
		unsigned char   errCode;

		if(m_bRead) 
		{
			fID = m_RfunctionID;
			Command = m_RCommand;
			// No COM communication. Read item handling to be done in getValue()
			if((fID == fNo) || (fID == fLastValue)|| (fID == fBusAttr) || (fID == fPortAttr) || (fID == fTraceAttr)) return m_initResult;			
		}
		else
		{
			fID = m_WfunctionID;
			Command = m_WCommand;
			// No COM communication.
			switch(fID) {
			case fBusAttr:
			case fPortAttr:
			case fNo:
				return m_initResult;
				break;
			default:
				break;
			}
		}

		// Transactions on COM 
		CMess.setTxDataByte(Command,0);
		CMess.setTxBytes(mess_lenghts[fID].TxBytes);
		CMess.setRxBytes(mess_lenghts[fID].RxBytes);
		switch(fID) {
		case fBVswitch:
		case fLVswitch:
			CMess.setTxDataByte(m_BranchID,1);
			break;
		case fB:
			if(!m_bRead)
			{
				if(m_bValues[0]) CMess.setTxDataByte('s',1);
				else CMess.setTxDataByte('u',1);
			}
			break;
		case fZ:
		case fH:
		case fQ:
			{
				CMess.setTxDataByte(m_CellSubAddr,1);
				CMess.setTxDataByte(m_BranchID,2);
				CMess.setTxDataByte(m_CellID,3);
				if(!m_bRead) //Write
				{
					result=getWriteWord(&writeWord, confVar);
					writeByte = (unsigned char)(writeWord & 0xFF);
					CMess.setTxDataByte(writeByte,4);
				}
			}
			break;
		case fCellMon:
			{
				char c[3];
				sprintf(c,"%hhu",m_BranchID);
				CMess.setTxDataByte(c[0],1);
				sprintf(c,"%02hhx", m_CellID);
				CMess.setTxDataByte(c[0],2);
				CMess.setTxDataByte(c[1],3);
				CMess.setTxDataByte('\r',4);
			}
			break;
		default:
			break;
		}
		if(result.isGood()) //pCMessage constructed
		{
			int i;
			bool send;
			result = m_pcp->waitCom();  // wait for ComPort Semaphore
			if(result.isNotGood()) 
			{
				printf("semaphore result: %s\n", result.toString().toUtf8());
				//m_pcp->getComPort()->postCom();
				return result;
			}
			if(fID == fB)
			{
				if(!m_bRead) // write
				{
					m_pcp->getComIO()->sendMessage(m_pcp->getPortHandle(), &CMess);
					result = CMess.getTxResult();
					m_semaphore.timedWait(10);        //Sleep(10);
					m_pcp->getComIO()->flushCOMPort(m_pcp->getPortHandle(), IO_IN);
				}
				else
				{
					result = OpcUa_Good;
				}
			}  // delay, no response

			else
			{
				errCode = m_pcp->getComIO()->sendRespMessage(m_pcp->getPortHandle(), &CMess, mess_lenghts[fID].ErrCodeType);
				if(CMess.getTxResult().isNotGood()) result = CMess.getTxResult();
				else if((mess_lenghts[fID].ErrCodeType>=0) && (errCode!=0)) result = getHVSysErrorCode(errCode);
				else if(CMess.getRxResult().isNotGood()) result = CMess.getRxResult();

			}   //  simple transaction or first message of complex transaction done

			if(!m_simple && result.isGood())	// complex write second message + exec	
			{
				writeByte = (unsigned char)((writeWord & 0xFF00)>>8);
				CMess.setTxDataByte(m_CellSubAddr+1,1);
				CMess.setTxDataByte(writeByte,4);

				errCode = m_pcp->getComIO()->sendRespMessage(m_pcp->getPortHandle(), &CMess, mess_lenghts[fID].ErrCodeType);  // send second message of multiwrite
				if(CMess.getTxResult().isNotGood()) result = CMess.getTxResult();
				else if((mess_lenghts[fID].ErrCodeType>=0) && (errCode!=0)) result = getHVSysErrorCode(errCode);
				else if(CMess.getRxResult().isNotGood()) result = CMess.getRxResult();

				if(result.isGood()) 
				{
					CMess.setTxDataByte(0,1);
					switch(m_CellSubAddr)
					{
					case 1:
						CMess.setTxDataByte(1,4); //voltage setpoint
						send = true;
						break;
					case 3:
						CMess.setTxDataByte(2,4); // threshold setpoint
						send = true;
						break;
					default:
						send = false;
						break;
					}
					if(send)         // send (write params -> execute)
					{
						errCode = m_pcp->getComIO()->sendRespMessage(m_pcp->getPortHandle(), &CMess, mess_lenghts[fID].ErrCodeType);
						if(CMess.getTxResult().isNotGood()) result = CMess.getTxResult();
						else if((mess_lenghts[fID].ErrCodeType>=0) && (errCode!=0)) result = getHVSysErrorCode(errCode);
						else if(CMess.getRxResult().isNotGood()) result = CMess.getRxResult();
					}
				}    // complex message (write params -> execute)
			}  // complex write second message + exec
			m_timestamp = time(NULL);
			m_pcp->postCom(); // release ComPort Semaphore
			if(result.isNotGood())
			{
				LOG(Log::WRN) << CMess.getTxDataByte(0) << " transaction failed." << " Result = " << result.toString().toUtf8();
			}
			else if(m_bRead)	// resolving return value
			{		
				switch(m_RfunctionID)
				{
				case fM:
					{
					for(i=0; i<8; i++)
					{
						if((CMess.m_RxData[0] & (0x01<<i)) ==0) m_bValues[i] = false;
						else m_bValues[i] = true;
						if((CMess.m_RxData[1] & (0x01<<i)) ==0) m_bValues[i+8] = false;
						else m_bValues[i+8] = true;
					}
					}//fM
					break;
				case fP:
					{
					for(i=0; i<4; i++)
					{
						m_fValues[i] = CMess.m_RxData[i] * (float)1.067;
					}

					for(i=4; i<8; i++)
					{
						m_fValues[i] = CMess.m_RxData[i] * (float)0.04;
					}
					} //fP
					break;
				case f3:
					{
					for(i=0; i<2; i++)
					{
						m_fValues[i] = CMess.m_RxData[i] * (float)1.067;
					}

					for(i=2; i<5; i++)
					{
						float x;
						x = (float)CMess.m_RxData[i];
						m_fValues[i] = - (float)0.00001*x*x*x + (float)0.004*x*x - (float)0.8816*x + (float)106.18;
					}
					}// f3
					break;
				case fH:
					{
					readWord = (unsigned int)CMess.m_RxData[1];
					switch(m_CellSubAddr)
					{
					case 7:
						for(i=0; i<8; i++)
						{
							if((CMess.m_RxData[1] & (0x01<<i)) ==0) 
								m_bValues[i] = false;
							else
								m_bValues[i] = true;
						}
						break;
					case 10:
					case 11:
						if(readWord == 0)
							m_fValues[0] = -1;
						else
							m_fValues[0] = (float)58.6 / readWord;
						break;
					case 14:
						if(readWord == 255)
							m_sValue = -1;
						else
							m_sValue = 50 * readWord;
						break;
					default:
						break;
					}
					} // fH
					break;
				case fQ:
					{
						readWord = ((unsigned int)CMess.m_RxData[2])<<8;
						readWord += CMess.m_RxData[1];
						switch(m_CellSubAddr)
						{
						case 1:
						case 12:
							m_fValues[0] = 500 + (readWord  & 0x3FF) * (float)1.465;
							break;
						case 3:
							m_fValues[0] = (readWord & 0x3FF) * (float)2.9326;
							break;
						case 5:
							m_fValues[0] = (readWord  & 0xFFF) * (float)0.526;
							break;
						case 8:
							m_fValues[0] = (readWord  & 0xFFF) * (float)0.683;
							break;
						default:
							break;
						}

						// calibration for read values
						switch(m_CalibType)
						{
						case setLinear:
							m_fValues[0] = (m_fValues[0] * m_Slope) + m_Offset;
							break;
						case monLinear:
							m_fValues[0] = (m_fValues[0] - m_Offset) / m_Slope;
							break;
						case noCalib:
						default:
							break;
						}
					}  // fQ
					break;
				case fCellMon: // HEX !!!!
					{
					// Status bits readWord = (unsigned int)CMess.m_RxData[0,1]; 
					char cstr[4];
					cstr[0]=CMess.m_RxData[0];
					cstr[1]=CMess.m_RxData[1];
					cstr[2]='\0';
					sscanf(cstr, "%hx", &readWord);
					for(i=0; i<8; i++)
					{
						if((readWord & (0x01<<i)) ==0) m_bValues[i] = false;
						else m_bValues[i] = true;
					}
					// Voltage readWord = (unsigned int)CMess.m_RxData[2,3,4]; kalibracja mon
					cstr[0]=CMess.m_RxData[2];
					cstr[1]=CMess.m_RxData[3];
					cstr[2]=CMess.m_RxData[4];
					cstr[3]='\0';
					sscanf(cstr, "%hx", &readWord);
					m_fValues[0] = (readWord  & 0xFFF) * (float)0.526;
					m_fValues[0] = (m_fValues[0] - m_Offset) / m_Slope;
					// Current readWord = (unsigned int)CMess.m_RxData[5,6,7];
					cstr[0]=CMess.m_RxData[5];
					cstr[1]=CMess.m_RxData[6];
					cstr[2]=CMess.m_RxData[7];
					cstr[3]='\0';
					sscanf(cstr, "%hx", &readWord);
					m_fValues[1] = (readWord  & 0xFFF) * (float)0.683;
					}  // fCellMon
					break;

				case fCellSet:    //   HEX !!!!
					{
					char cstr[4];
					// Voltage readWord = (unsigned int)CMess.m_RxData[0,1,2]; kalibracja set
					cstr[0]=CMess.m_RxData[0];
					cstr[1]=CMess.m_RxData[1];
					cstr[2]=CMess.m_RxData[2];
					cstr[3]='\0';
					sscanf(cstr, "%hx", &readWord);
					m_fValues[0] = 500 + (readWord  & 0x3FF) * (float)1.465;
					m_fValues[0] = (m_fValues[0] * m_Slope) + m_Offset;

					// Threshold readWord = (unsigned int)CMess.m_RxData[3,4,5]; 
					cstr[0]=CMess.m_RxData[3];
					cstr[1]=CMess.m_RxData[4];
					cstr[2]=CMess.m_RxData[5];
					cstr[3]='\0';
					sscanf(cstr, "%hx", &readWord);
					m_fValues[0] = (readWord & 0x3FF) * (float)2.9326;

					// Rup readWord = (unsigned int)CMess.m_RxData[6,7];
					cstr[0]=CMess.m_RxData[6];
					cstr[1]=CMess.m_RxData[7];
					cstr[2]='\0';
					sscanf(cstr, "%hx", &readWord);
					if(readWord == 0) m_fValues[0] = -1;
					else m_fValues[0] = (float)58.6 / readWord;

					// Rdn readWord = (unsigned int)CMess.m_RxData[8,9];
					cstr[0]=CMess.m_RxData[8];
					cstr[1]=CMess.m_RxData[9];
					cstr[2]='\0';
					sscanf(cstr, "%hx", &readWord);
					if(readWord == 0) m_fValues[0] = -1;
					else m_fValues[0] = (float)58.6 / readWord;

					// Stdby readWord = (unsigned int)CMess.m_RxData[10,11,12];
					cstr[0]=CMess.m_RxData[10];
					cstr[1]=CMess.m_RxData[11];
					cstr[2]=CMess.m_RxData[12];
					cstr[3]='\0';
					sscanf(cstr, "%hx", &readWord);
					m_fValues[0] = 500 + (readWord  & 0x3FF) * (float)1.465;

					// ProtDelay readWord = (unsigned int)CMess.m_RxData[13,14];
					cstr[0]=CMess.m_RxData[13];
					cstr[1]=CMess.m_RxData[14];
					cstr[2]='\0';
					sscanf(cstr, "%hx", &readWord);
					m_fValues[0] = (readWord & 0x3FF) * (float)2.9326;
					}  //fCellSet
					break;

				case fCellAll:  //HEX !!!!
					{
					char cstr[4];
					// Voltage Set readWord = (unsigned int)CMess.m_RxData[0,1,2]; kalibracja set !!!!
					cstr[0]=CMess.m_RxData[0];
					cstr[1]=CMess.m_RxData[1];
					cstr[2]=CMess.m_RxData[2];
					cstr[3]='\0';
					sscanf(cstr, "%hx", &readWord);
					m_fValues[0] = 500 + (readWord  & 0x3FF) * (float)1.465;
					m_fValues[0] = (m_fValues[0] * m_Slope) + m_Offset;

					// Threshold readWord = (unsigned int)CMess.m_RxData[3,4,5]; 
					cstr[0]=CMess.m_RxData[3];
					cstr[1]=CMess.m_RxData[4];
					cstr[2]=CMess.m_RxData[5];
					cstr[3]='\0';
					sscanf(cstr, "%hx", &readWord);
					m_fValues[0] = (readWord & 0x3FF) * (float)2.9326;

					// Rup readWord = (unsigned int)CMess.m_RxData[6,7];
					cstr[0]=CMess.m_RxData[6];
					cstr[1]=CMess.m_RxData[7];
					cstr[2]='\0';
					sscanf(cstr, "%hx", &readWord);
					if(readWord == 0) m_fValues[0] = -1;
					else m_fValues[0] = (float)58.6 / readWord;

					// Rdn readWord = (unsigned int)CMess.m_RxData[8,9];
					cstr[0]=CMess.m_RxData[8];
					cstr[1]=CMess.m_RxData[9];
					cstr[2]='\0';
					sscanf(cstr, "%hx", &readWord);
					if(readWord == 0) m_fValues[0] = -1;
					else m_fValues[0] = (float)58.6 / readWord;

					// Stdby readWord = (unsigned int)CMess.m_RxData[10,11,12];
					cstr[0]=CMess.m_RxData[10];
					cstr[1]=CMess.m_RxData[11];
					cstr[2]=CMess.m_RxData[12];
					cstr[3]='\0';
					sscanf(cstr, "%hx", &readWord);
					m_fValues[0] = 500 + (readWord  & 0x3FF) * (float)1.465;

					// ProtDelay readWord = (unsigned int)CMess.m_RxData[13,14];
					cstr[0]=CMess.m_RxData[13];
					cstr[1]=CMess.m_RxData[14];
					cstr[2]='\0';
					sscanf(cstr, "%hx", &readWord);
					m_fValues[0] = (readWord & 0x3FF) * (float)2.9326;	

					// Status bits readWord = (unsigned int)CMess.m_RxData[0,1];
					cstr[0]=CMess.m_RxData[15];
					cstr[1]=CMess.m_RxData[16];
					cstr[2]='\0';
					sscanf(cstr, "%hx", &readWord);
					for(i=0; i<8; i++)
					{
						if((readWord & (0x01<<i)) ==0) m_bValues[i] = false;
						else m_bValues[i] = true;
					}

					// Voltage readWord = (unsigned int)CMess.m_RxData[2,3,4]; kalibracja mon !!!!
					cstr[0]=CMess.m_RxData[17];
					cstr[1]=CMess.m_RxData[18];
					cstr[2]=CMess.m_RxData[19];
					cstr[3]='\0';
					sscanf(cstr, "%hx", &readWord);
					m_fValues[0] = (readWord  & 0xFFF) * (float)0.526;
					m_fValues[0] = (m_fValues[0] - m_Offset) / m_Slope;

					// Current readWord = (unsigned int)CMess.m_RxData[5,6,7];
					cstr[0]=CMess.m_RxData[20];
					cstr[1]=CMess.m_RxData[21];
					cstr[2]=CMess.m_RxData[22];
					cstr[3]='\0';
					sscanf(cstr, "%hx", &readWord);
					m_fValues[1] = (readWord  & 0xFFF) * (float)0.683;
					} //fCellAll
					break;
				default:
					break;
				}
			}   // resolving return value
			if(!m_bRead) {
				UaDateTime sdt(time(NULL));
				m_lastValue.setDataValue(*confVar,OpcUa_False,result.statusCode(),sdt,sdt);
			}
		}
		LOG(Log::TRC) << CMess.getTxDataByte(0) << " Transaction result: " << result.toString().toUtf8();
		return result;
	}

	UaStatus ComTransaction::getWriteWord(unsigned short *wWord, UaVariant *confVar)
	{
		UaStatus	result = OpcUa_Good;
		OpcUa_Float fdata = 0;

		if(m_WCommand != 'Z') return result;
		
		switch(m_CellSubAddr) 
		{

		//***************************************************************Cell switch ON/OFF
		case 0:
			if(m_bValues[0] == false) *wWord = 5;
			else *wWord = 4;
			break;
		//***************************************************************HVSetpoint
		//***************************************************************StandbyVoltSetpoint
		case 1:
		case 12:
			switch(m_CalibType) {  
			case setLinear:
				m_fValues[0] = (m_fValues[0] - m_Offset)/m_Slope;
				break;
			case monLinear:
			case noCalib:
			default:
				break;
			}    // calibration for write values
			if(m_fValues[0] <= 501.4649)
			{
				*wWord =0;
				fdata = (OpcUa_Float)0.000;
			}      // min setpoint when data out of range, special value for trip to switch the cell off

			if(m_fValues[0] > 1998.695)
			{
				*wWord = 0x3FF;
				fdata = (OpcUa_Float)1998.695;
				if(m_CalibType == setLinear)
				{
					fdata = ((OpcUa_Float)1998.695 * m_Slope) + m_Offset;
				} // calibration for confirmation value
			}     // max setpoint if value out of range

			if((m_fValues[0] > 501.4649) && (m_fValues[0] <= 1998.695))
			{
				*wWord = (unsigned short)((m_fValues[0]-499.9999 + 0.7325)/1.465);  // in Volts (1/2 digitalization step added for better value approximation)
				fdata = (OpcUa_Float)*wWord * (OpcUa_Float)1.465 + (OpcUa_Float)499.9999;
				if(m_CalibType == setLinear)  // calibration for confirmation value
				{
					fdata = fdata* m_Slope + m_Offset;
				}
			}
			confVar->setFloat(fdata);
			break;

		//***************************************************************ThresholdSetpoint
		case 3:
			if(m_fValues[0] < 2.9326)
			{
				*wWord = 0;
				fdata = (OpcUa_Float)0.000;
				}
			if(m_fValues[0] >= 3000.0498)
			{
				*wWord = 0x3FF;
				fdata = (OpcUa_Float)3000.0498;
			}
			if((m_fValues[0] >= 2.9326) && (m_fValues[0] < 3000.0498))
			{
				*wWord = (unsigned short)((m_fValues[0] + 1.4663)/2.9326); // in uA(1/2 digitalization step added for better value approximation)
				fdata = (OpcUa_Float)*wWord * (OpcUa_Float)2.9326;
			}
			confVar->setFloat(fdata);
			break;

		//***************************************************************RUpSetpoint
		//***************************************************************RDnSetpoint
		case 10:
		case 11:
			if(m_fValues[0] == (OpcUa_Float)-1.0)
			{
				*wWord =0;
				fdata = (OpcUa_Float)-1.000;
			}

			else if(m_fValues[0] <= 0.2298)
			{
				*wWord = 255;
				fdata = (OpcUa_Float)0.2298;
			}
			
			if(m_fValues[0] >= 58.6)
			{
				*wWord = 1;
				fdata = (OpcUa_Float)58.6;
			}

			if((m_fValues[0] > 0.2298) && (m_fValues[0] < 58.6))
			{
				*wWord = (unsigned short)(1465/(m_fValues[0] * 25)); // in V/s
				fdata = 1465/((OpcUa_Float)*wWord * 25);
			}
			confVar->setFloat(fdata);
			break;
		
		//***************************************************************ProtectionDelaySetpoint
		case 14:
			if(m_sValue == -1)
			{
				*wWord = 0xFF;
				fdata = (OpcUa_Float)-1.000;
			}
			else if(m_sValue < 50)
			{
				*wWord = 0;
				fdata = (OpcUa_Float)0.000;
			}
			if(m_sValue > 12700)
			{
				*wWord = 0xFE;
				fdata = (OpcUa_Float)12700.000;
			}
			if((m_sValue >= 50) && (m_sValue <= 12700))
			{
				*wWord = (unsigned short)((m_sValue+25)/50); // in ms(1/2 digitalization step added for better value approximation)
				fdata = (OpcUa_Float)(*wWord * 50.0);
			}
			confVar->setFloat(fdata);
			break;

		default:
			result = OpcUa_BadNotSupported;
			break;
		}
		//printf("fdata = %f\n", fdata);
		return result;
	}

	UaStatus ComTransaction::getHVSysErrorCode(unsigned char err)
	{
		UaStatus			result;

		switch(err)
		{
		case '0':
		case 0:
			result = OpcUa_Good;
			break;
		case '1':
		case 1:
			result = OpcUa_BadDeviceFailure;// HVSYS_E_MESS_NOACK;
			LOG(Log::ERR) << "No acknowledge from Cell " << m_PortID.toUtf8() << ".Branch" << (int)m_BranchID << ".Cell"<< (int)m_CellID;
			break;
		case '2':
		case 2:
			result = OpcUa_BadDeviceFailure;// HVSYS_E_MESS_ARBLOST;
			LOG(Log::ERR) << "Arbitration lost on system bus " << m_PortID.toUtf8() << ".Branch" << (int)m_BranchID << ".Cell"<< (int)m_CellID;
			break;
		case '3':
		case 3:
			result = OpcUa_BadDeviceFailure;// HVSYS_E_MESS_BUSSTUCK;
			LOG(Log::ERR) << "Bus is stuck " << m_PortID.toUtf8() << ".Branch" << (int)m_BranchID << ".Cell"<< (int)m_CellID;
			break;
		case '4':
		case 4:
			result = OpcUa_BadDeviceFailure;// HVSYS_E_MESS_BUSTIMEOUT;
			LOG(Log::ERR) << "Timeout on the bus " << m_PortID.toUtf8() << ".Branch" << (int)m_BranchID << ".Cell"<< (int)m_CellID;
			break;
		case '5':
		case 5:
			result = OpcUa_BadDeviceFailure;// HVSYS_E_MESS_NOBRANCH_NOLV;
			LOG(Log::ERR) << "Branch or LV channel doesn't exist " << m_PortID.toUtf8() << ".Branch" << (int)m_BranchID << ".Cell"<< (int)m_CellID;
			break;
		case '6':
		case 6:
			result = OpcUa_BadDeviceFailure; // HVSYS_E_MESS_TEMP_PROT;
			LOG(Log::ERR) << "Temperature protection - can't switch BV on " << m_PortID.toUtf8() << ".Branch" << (int)m_BranchID << ".Cell"<< (int)m_CellID;
			break;
		case '7':
		case 7:
			result = OpcUa_BadDeviceFailure;// HVSYS_E_MESS_LV_PROT;
			LOG(Log::ERR) << "LV protection - can't switch BV on " << m_PortID.toUtf8() << ".Branch" << (int)m_BranchID << ".Cell"<< (int)m_CellID;
			break;
		case '8':
		case 8:
			result = OpcUa_BadDeviceFailure; // HVSYS_E_MESS_UNKNOWN_COMMAND;
			LOG(Log::ERR) << "Unknown command " << m_PortID.toUtf8() << ".Branch" << (int)m_BranchID << ".Cell"<< (int)m_CellID;
			break;
		case '9':
		case 9:
			result = OpcUa_BadDeviceFailure;// HVSYS_E_MESS_BRANCHTIMEOUT;
			LOG(Log::ERR) << "Timeout in communication with branch controller " << m_PortID.toUtf8() << ".Branch" << (int)m_BranchID << ".Cell"<< (int)m_CellID;
			break;
		default:
			result = OpcUa_BadDeviceFailure; // HVSYS_E_MESS_UNKNOWN_ERRCODE;
			LOG(Log::ERR) << "Unknown error code: " << (int)err;
			break;
		}
		return result;
	}

	void ComTransaction::setResponseID()
	{
		if(m_bRead)
			{
				switch(m_RfunctionID) {
				case fP:
					m_responseID = rP;
					break;
				case f3:
					m_responseID = r3;
					break;
				case fM:
					m_responseID = rM;
					break;
				case fH:
					{
						switch(m_CellSubAddr) {
						case 7:
							m_responseID = rH07;
							break;
						case 10:
							m_responseID = rH10;
							break;
						case 11:
							m_responseID = rH11;
							break;
						case 14:
							m_responseID = rH14;
							break;
						default:
							m_responseID = rNo;
							break;
						}
					}
					break;
				case fQ:
					{
						switch(m_CellSubAddr) {
						case 1:
							m_responseID = rQ01;
							break;
						case 3:
							m_responseID = rQ03;
							break;
						case 5:
							m_responseID = rQ05;
							break;
						case 8:
							m_responseID = rQ08;
							break;
						case 12:
							m_responseID = rQ12;
							break;
						default:
							m_responseID = rNo;
							break;
						}
					}
					break;
				case fCellMon:
					m_responseID = rCellMon;
					break;
				case fCellSet:
					m_responseID = rCellSet;
					break;
				case fCellAll:
					m_responseID = rCellAll;
					break;
				case fBusAttr:
				case fPortAttr:
				case fTraceAttr:
				case fNo:
				case fLastValue:
				default:
					m_responseID = rNo;
					break;
			}
		}
		else m_responseID = rNo;
	}
	void ComTransaction::updateValues(UaNode *parent, ItemsDefs *pidefs, UaStatus *result)
	{
		UaVariant value;
		UaDataValue dataValue;
		int index = 0;
		// ItemsDefs *pidefs = m_pcp->m_idefs;
		UaString itemID, nodeID;
		UaReferenceLists *list;
		UaNode* node;
		UaString uaStr;
		UaVariable* targetValue;
		time_t t_now = time(NULL);
		//OpcUa::BaseDataVariableType* targetValue; //*JO* inna metoda ??


		typedef multimap<RESPONSE_ID,RESPONSE_ITEM_ATTRIBUTES>::iterator rait;
		pair<rait,rait> range;
	
		if(pidefs->respItems.size() > 0) 	
		{
			range = pidefs->respItems.equal_range(m_responseID);
			rait it;
			for(it = range.first; it!=range.second; it++)
			{
				index = it->second.dataIndex();
				itemID = it->second.itemID().c_str();
				nodeID=it->second.nodeID().c_str();
				switch(it->second.varType()) 
				{
				case OpcUaType_Boolean:
					value.setBool((OpcUa_Boolean)getBoolVal(index));
					//printf("bool value: %d\n", getBoolVal(index));
					break;
				case OpcUaType_Int16:
					value.setInt16((OpcUa_Int16)getShortVal());
					//printf("int value: %d\n", getShortVal());
					break;
				case OpcUaType_Float:
					value.setFloat((OpcUa_Float)getFloatVal(index));
					//printf("float value: %f\n", getFloatVal(index));
					break;
				default:
					break;
				}
				list = parent->getUaReferenceLists();
				if(strcmp(nodeID.toUtf8(), ""))
				{
					node = list->getTargetNodeByBrowseName(UaQualifiedName(UaString(nodeID), parent->browseName().namespaceIndex()));
					if(node != NULL) list = node->getUaReferenceLists();
				}
				node = list->getTargetNodeByBrowseName(UaQualifiedName(UaString(itemID), parent->browseName().namespaceIndex()));
				if(node != NULL) 
				{
					uaStr = node->nodeId().identifierString();
					//printf("found targetNode: %s\n",uaStr.toUtf8());
					dataValue.setValue(value, OpcUa_False,OpcUa_Good); 
					dataValue.setStatusCode(result->statusCode());
					UaDateTime sdt(t_now);
					dataValue.setSourceTimestamp(sdt);
					targetValue = (UaVariable*)node;    //*JO* inna metoda ??
					targetValue->setValue(0,dataValue, OpcUa_False);      // *JO* inna metoda ??
				}
			} 
		}
	}

	UaVariant ComTransaction::getValue(ItemsDefs *pidefs)
	{
		UaVariant value;
		UaDataValue dataVal;
		UaDateTime sTS;
		int index = 0;
//		ItemsDefs *pidefs = m_pco->m_idefs;  *JO* moved to function argument
		//UaDataValue *pVar = (UaDataValue *)this;

		switch(m_RfunctionID )
		{
		case fBusAttr: // data from ComPort Object
			if(m_pcp->getCommBus() == USB) value = "USB";
			if(m_pcp->getCommBus() == RS232) value = "RS232";
			break;

		case fLastValue:  // last written value
			switch(m_itemType)
			{
			case OpcUaType_Boolean:
				value.setBool(m_lastValue.value()->Value.Boolean);
				break;
			case OpcUaType_Int16:
				value.setInt16(m_lastValue.value()->Value.Int16);
				break;
			case OpcUaType_Float:
				value.setFloat(m_lastValue.value()->Value.Float);
				break;
			default:
				break;
			}
			sTS = m_lastValue.sourceTimestamp();
			m_timestamp = sTS.toTime_t();
			break;

		default:  // data from hardware transaction
			if(pidefs->respItems.size() > 0)
			{
				std::multimap<RESPONSE_ID,RESPONSE_ITEM_ATTRIBUTES>::iterator rait = pidefs->respItems.begin();
				rait = pidefs->respItems.find(m_responseID);
				if(rait != pidefs->respItems.end()) 
				{
					index = rait->second.dataIndex();
					switch(rait->second.varType())
					{
					case OpcUaType_Boolean:
						value = getBoolVal(index);
						//printf("get bool value: %d\n", getBoolVal(index));
					break;
					case OpcUaType_Int16:
						value = getShortVal();
						//printf("get short value: %d\n", getShortVal());
						break;
					case OpcUaType_Float:
						value = getFloatVal(index);
						//printf("get float value: %f\n", getFloatVal(index));
						break;
					default:
						break;
					}
				}
			}
			break;
		}
		return value;
	}

