#include "ComPortObject.h"

// namespace AddressSpace
// {
	ComPortObject::ComPortObject(const Configuration::PORT  &config, CComDriver *cd): m_comSemaphore(0,1)
	{
		setCommBus(config.CommunicationBus().c_str());					// "RS232"
		m_sPortID = config.PortID().c_str();							// "COM3"
		setBaudRate(config.BaudRate());									// "19200"
		setDataBits(config.DataBits());									// "8"
		setStopBits(config.StopBits());									// "1"
		setParity(config.Parity());									// "No"
		setControllerType(TYPE_SC508);									// SC508 powinno byc w Crate tbdl
		m_uiRit = 0;
		m_uiRttc = 50;
		m_uiRttm = 0;
		m_uiWttc = 50;
		m_uiWttm = 0;
		m_pcd = cd;

		UaStatus result = iniPort(); // CreateFile(),SetupComm(),setCOMstate(), setCOMTimeouts()
		if(result.isGood())
		{
			LOG(Log::INF) << "COM Port is initialized: " << config.PortID().c_str();
		}
		else
		{
			LOG(Log::ERR) << "COM Port initialization failed: " << config.PortID().c_str();
		}
		// first usage of semaphore returns OpcUa_GoodNonCriticalTimeout
		m_comSemaphore.timedWait(1);
		m_comSemaphore.post(1);
	}

	ComPortObject::~ComPortObject(void)
	{
	}


	UaStatus ComPortObject::setCommBus(const string& sBus)
	{
		int result;
		UaStatus ret;

		if((result = m_pcd->getCOMParameter(sBus, COMM_BUS)) != -1)
		{
			m_iBaud = (OpcUa_Int32)result;
			ret = OpcUa_Good;
		}
		else ret = OpcUa_BadInvalidArgument;
		return ret;
	};

	UaStatus ComPortObject::setBaudRate(const string& sbaudrate)
	{
		int result;
		UaStatus ret;

		if((result = m_pcd->getCOMParameter(sbaudrate, BAUD_RATE)) != -1)
		{
			m_iBaud = (OpcUa_Int32)result;
			ret = OpcUa_Good;
		}
		else ret = OpcUa_BadInvalidArgument;
		return ret;
	};


	UaStatus ComPortObject::setDataBits(const string& sdatabits)
	{
		int result;

		if((result = m_pcd->getCOMParameter(sdatabits, DATA_BITS)) != -1)
		{
			m_bData = (OpcUa_Byte)result;
			return OpcUa_Good;
		}
		else return OpcUa_BadInvalidArgument;
	};

	UaStatus ComPortObject::setStopBits(const string& sstopbits)
	{
		int result;

		if((result = m_pcd->getCOMParameter(sstopbits, STOP_BITS)) != -1)
		{
			m_bStop = (OpcUa_Byte)result;
			return OpcUa_Good;
		}
		else return OpcUa_BadInvalidArgument;
	};

	UaStatus ComPortObject::setParity(const string& sparity)
	{
		int result;

		if((result = m_pcd->getCOMParameter(sparity, PARITY)) != -1)
		{
			m_bParity = (OpcUa_Byte)result;
			return OpcUa_Good;
		}
		else return OpcUa_BadInvalidArgument;
	};

	UaStatus ComPortObject::setControllerType(const string& controllertype)
	{
		int result;

		if((result = m_pcd->getCOMParameter(controllertype, CONTROLLER_TYPE)) != -1)
		{
			m_bController = (OpcUa_Byte)result;
			return OpcUa_Good;
		}
		else return OpcUa_BadInvalidArgument;
	};

	UaStatus ComPortObject::setReadIntervalTimeout(const string& sReadIntervalTimeout)
	{
		int result;

		if((result = m_pcd->getCOMParameter(sReadIntervalTimeout, READINTERVALTIMEOUT)) != -1)
		{
			m_uiRit = (OpcUa_UInt32)result;
			return OpcUa_Good;
		}
		else return OpcUa_BadInvalidArgument;
	};
	
	UaStatus ComPortObject::setReadTotalTimeoutConstant(const string& sReadTotalTimeoutConstant)
	{
		int result;

		if((result = m_pcd->getCOMParameter(sReadTotalTimeoutConstant, READTOTALTIMEOUTCONSTANT)) != -1)
		{
			m_uiRttc = (OpcUa_UInt32)result;
			return OpcUa_Good;
		}
		else return OpcUa_BadInvalidArgument;
	};
	
	UaStatus ComPortObject::setReadTotalTimeoutMultiplier(const string& sReadTotalTimeoutMultiplier)
	{
		int result;

		if((result = m_pcd->getCOMParameter(sReadTotalTimeoutMultiplier, READTOTALTIMEOUTMULTIPLIER)) != -1)
		{
			m_uiRttm = (OpcUa_UInt32)result;
			return OpcUa_Good;
		}
		else return OpcUa_BadInvalidArgument;
	};
	
	UaStatus ComPortObject::setWriteTotalTimeoutConstant(const string& sWriteTotalTimeoutConstant)
	{
		int result;

		if((result = m_pcd->getCOMParameter(sWriteTotalTimeoutConstant, WRITETOTALTIMEOUTCONSTANT)) != -1)
		{
			m_uiWttc = (OpcUa_UInt32)result;
			return OpcUa_Good;
		}
		else return OpcUa_BadInvalidArgument;
	};
	
	UaStatus ComPortObject::setWriteTotalTimeoutMultiplier(const string& sWriteTotalTimeoutMultiplier)
	{
		int result;

		if((result = m_pcd->getCOMParameter(sWriteTotalTimeoutMultiplier, WRITETOTALTIMEOUTMULTIPLIER)) != -1)
		{
			m_uiWttm = (OpcUa_UInt32)result;
			return OpcUa_Good;
		}
		else return OpcUa_BadInvalidArgument;
	};

	UaStatus ComPortObject::iniPort()
	{
		int hPort;

		UaStatus result = m_pcd->iniPort(getName().toUtf8(), getBaudRate(), getDataBits(), getParity(), getStopBits(), 
										 getReadIntervalTimeout(), getReadTotalTimeoutConstant(), getReadTotalTimeoutMultiplier(), 
										 getWriteTotalTimeoutConstant(), getWriteTotalTimeoutMultiplier(), &hPort);

		if(result.isGood())
		{
			m_hPortHandle = hPort;
		}
		return result;
	}
	
	UaStatus ComPortObject::terminate(void)
	{
		UaStatus result;
		m_pcd->flushCOMPort(m_hPortHandle, IO_INOUT);
		result = m_pcd->terminatePort(m_hPortHandle);
		if(result.isGood())
		{
			LOG(Log::INF) << m_sPortID.toUtf8() << " terminated";
		}
		else
		{
			LOG(Log::ERR) << m_sPortID.toUtf8() << " termination failed";
		}
		return result;
	}
// }
