
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.

    The stub of this file was generated by Quasar (additional info: using transform designToDeviceHeader.xslt)
    on 2019-04-05T16:32:39.76+02:00

    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.



 */





#ifndef __DHVROOT__H__
#define __DHVROOT__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DHVROOT.h>


namespace Device
{




class
    DHVROOT
    : public Base_DHVROOT
{

public:
    /* sample constructor */
    explicit DHVROOT (
        const Configuration::HVROOT & config,
        Parent_DHVROOT * parent
    ) ;
    /* sample dtr */
    ~DHVROOT ();




    /* delegators for
    cachevariables and sourcevariables */


    /* delegators for methods */


private:
    /* Delete copy constructor and assignment operator */
    DHVROOT( const DHVROOT & );
    DHVROOT& operator=(const DHVROOT &other);

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:

private:


};





}

#endif // include guard
