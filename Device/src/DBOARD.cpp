
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.

    The stub of this file was generated by Quasar (additional info: using transform designToDeviceBody.xslt)
    on 2019-04-05T17:05:33.854+02:00

    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.



 */




#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DBOARD.h>
#include <ASBOARD.h>
#include <DBRANCH.h>
#include <ASBRANCH.h>
#include <DCELL.h>
#include <ASCELL.h>



namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DBOARD::DBOARD (
    const Configuration::BOARD & config,
    Parent_DBOARD * parent
):
    Base_DBOARD( config, parent)

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
    m_pcp = getParent()->getComPortObject();
	idefs = new ItemsDefs(Ba_CellType);
	m_portID = parent->getPortID();
	m_branchID = parent->getBranchID();
	m_boardID = config.name();
	m_enable = OpcUa_True;
	m_monEmptyCycles = 6 * MON_EMPTY_CYCLES_CONST;
	m_runThread = true;
	m_idThread = pthread_create(&m_hThread, NULL, &monThread, (void*)this);
}

/* sample dtr */
DBOARD::~DBOARD ()
{
	m_enable = OpcUa_False;
	m_runThread = false;
	/*pthread_join(m_hThread,0);
	LOG(Log::INF) << m_portID.c_str() << " , " << m_branchID.c_str() << " " << m_boardID.c_str() << " board MonThread stopped";*/
	delete idefs;
}

/* delegators for cachevariables and externalvariables */

/* Note: never directly call this function. */

UaStatus DBOARD::writeMonThreadEnable ( const OpcUa_Boolean & v)
{
    if(v==OpcUa_True)
    {
    	enableMonThread();
    	LOG(Log::INF) << m_portID.c_str() << " , " << m_branchID.c_str() << " " << m_boardID.c_str() << " board MonThread enabled";
    }
    else
    {
    	disableMonThread();
    	LOG(Log::INF) << m_portID.c_str() << " , " << m_branchID.c_str() << " " << m_boardID.c_str() << " board MonThread disabled";
    }
    return OpcUa_Good;
}


// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333



void* DBOARD::monThread(void *pBoard)
{
	UaStatus 								result = OpcUa_BadWaitingForInitialData;
	UaVariant 								var;
    DBOARD 									*board = static_cast<DBOARD *>(pBoard);
    DBRANCH 								*branch;
    DCELL									*cell;
    int 									i, nBrCells, nBoCells, iEmptyCycles;
    unsigned char 							bo, cc;
    ComTransaction							*tr;
    std::vector <DCELL *>					mon_Cells;


    sscanf(&board->getBoardID()[4], "%hhu", &bo);
    branch = board->getParent();
    tr = new ComTransaction(branch->getComPortObject());

    LOG(Log::INF) << board->getPortID() << " , " << board->getBranchID() << " " << board->getBoardID() << " board MonThread start";

    usleep(1000000);

    sscanf(&board->getParent()->getBranchID()[4], "%hhu", &bo);
    nBrCells = (int)board->getParent()->cells().size();
    for( i=0; i< nBrCells; i++)
	{
		cc=board->getParent()->cells().at(i)->get_cc();
		if( (cc > 21*(bo-1)) && (cc <= 21*(bo)) )
		{
			cell = board->getParent()->cells().at(i);
			mon_Cells.push_back(cell);
		}
	}
    LOG(Log::TRC) << board->getPortID() << " , " << board->getBranchID() << " " << board->getBoardID() << " board has " << mon_Cells.size() << " Cells";
    iEmptyCycles = (bo-1) * MON_EMPTY_CYCLES_CONST;     // initial delay to spread mon threads start for different boards

    while(board->getRunThread())	// returns m_runThread
	{
		if(iEmptyCycles > 0)
		{
			usleep(MON_EMPTY_SLEEP_TIME);   // wake up each cycle to check if thread has to stop
			iEmptyCycles --;
		}
		else
		{
			if(board->getEnable() != OpcUa_False)	// hardware access enabled
			{
				LOG(Log::TRC) << board->getPortID() << " , " << board->getBranchID()
							  << " " << board->getBoardID() << " MonThread fCellMon transactions";
				nBoCells = (int)mon_Cells.size();
				for( i=0; i< nBoCells; i++)
				{
					cell = mon_Cells.at(i);
					tr->initMonTransaction(cell->get_cb(), cell->get_cc(),fCellMon);
					tr->setCalib(cell->get_setSlope(), cell->get_setOffset(),
								 cell->get_monSlope(), cell->get_monOffset());
					result = tr->processComTransaction(&var);
					cell->getAddressSpaceLink()->setError(tr->getBoolVal(0),result.statusCode(), UaDateTime::now());
					cell->getAddressSpaceLink()->setGeneratorStatus(tr->getBoolVal(1),result.statusCode(), UaDateTime::now());
					cell->getAddressSpaceLink()->setAccumError(tr->getBoolVal(2),result.statusCode(), UaDateTime::now());
					cell->getAddressSpaceLink()->setCurrentOverload(tr->getBoolVal(3),result.statusCode(), UaDateTime::now());
					cell->getAddressSpaceLink()->setStandbyStatus(tr->getBoolVal(4),result.statusCode(), UaDateTime::now());
					cell->getAddressSpaceLink()->setRampDownStatus(tr->getBoolVal(6),result.statusCode(), UaDateTime::now());
					cell->getAddressSpaceLink()->setRampUpStatus(tr->getBoolVal(7),result.statusCode(), UaDateTime::now());
					cell->getAddressSpaceLink()->setVoltageValue(tr->getFloatVal(0),result.statusCode(), UaDateTime::now());
					cell->getAddressSpaceLink()->setCurrentValue(tr->getFloatVal(1),result.statusCode(), UaDateTime::now());
				}
				// usleep(MON_EMPTY_SLEEP_TIME);
			}
			else LOG(Log::INF) << board->getPortID() << " , " << board->getBranchID() << " "
					           << board->getBoardID() << " hardware access disabled";

			iEmptyCycles = board->getMonEmptyCycles();
		}
	}
    delete tr;
    LOG(Log::INF) << board->getPortID() << " , " << board->getBranchID() << " " << board->getBoardID()  << " board exiting MonThread" << endl;
    pthread_exit(NULL);
    return NULL;
	}
void DBOARD::stopMonThread(void)
{
	 m_runThread = false;
	 pthread_join(m_hThread,0);
	 LOG(Log::INF) << m_portID.c_str() << " , " << m_branchID.c_str() << " " << m_boardID.c_str() << " Board MonThread exited" << endl;
	 return;
}

}



